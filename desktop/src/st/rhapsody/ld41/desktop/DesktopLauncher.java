package st.rhapsody.ld41.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import st.rhapsody.ld41.LD41;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 1280;
		config.height = 960;
		config.title = "GolfZ - By Nicklas Lof and Mathias Lindfeldt for Ludum Dare 41";
		new LwjglApplication(new LD41(), config);
	}
}
