package st.rhapsody.ld41.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import st.rhapsody.ld41.level.Level;
import st.rhapsody.ld41.resource.Sprites;

public class InputController implements InputProcessor{
    private float screenX;
    private float screenY;
    private Vector3 position = new Vector3();
    private boolean spacePressed;
    private boolean leftPressed;
    private boolean rightPressed;
    private boolean upPressed;
    private boolean downPressed;
    private int counter;

    public InputController() {
        screenX = 0;
        screenY = Gdx.graphics.getHeight();
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.SPACE) {
            this.spacePressed = true;
        }

        if (keycode == Input.Keys.D || keycode == Input.Keys.RIGHT) {
            this.rightPressed = true;
        }

        if (keycode == Input.Keys.A || keycode == Input.Keys.LEFT) {
            this.leftPressed = true;
        }

        if (keycode == Input.Keys.W || keycode == Input.Keys.UP) {
            this.upPressed = true;
        }

        if (keycode == Input.Keys.S || keycode == Input.Keys.DOWN) {
            this.downPressed = true;
        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Input.Keys.SPACE) {
            this.spacePressed = false;
        }

        if (keycode == Input.Keys.D || keycode == Input.Keys.RIGHT) {
            this.rightPressed = false;
        }

        if (keycode == Input.Keys.A || keycode == Input.Keys.LEFT) {
            this.leftPressed = false;
        }

        if (keycode == Input.Keys.W || keycode == Input.Keys.UP) {
            this.upPressed = false;
        }

        if (keycode == Input.Keys.S || keycode == Input.Keys.DOWN) {
            this.downPressed = false;
        }


        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        this.spacePressed = true;
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        this.spacePressed = false;
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        this.screenX = screenX;
        this.screenY = screenY;
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        this.screenX = screenX;
        this.screenY = screenY;
        return true;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public void renderCrosshair(SpriteBatch spriteBatch, OrthographicCamera camera){

        if (Gdx.input.getX() > Gdx.graphics.getWidth()-16){
            Gdx.input.setCursorPosition(Gdx.graphics.getWidth()-16,Gdx.input.getY());
        }

        if (Gdx.input.getX() < 0){
            Gdx.input.setCursorPosition(0,Gdx.input.getY());
        }


        if (Gdx.input.getY() > Gdx.graphics.getHeight()){
            Gdx.input.setCursorPosition(Gdx.input.getX(), Gdx.graphics.getHeight());
        }

        if (Gdx.input.getY() < 16){
            Gdx.input.setCursorPosition(Gdx.input.getX(), 16);
        }

        Vector3 unproject = camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));

        Sprites.crosshair.setPosition(unproject.x, unproject.y);
        Sprites.crosshair.draw(spriteBatch);


    }

    public Vector3 getProjectedPosition(OrthographicCamera camera){
        position.set(this.screenX, this.screenY, 1);
        Vector3 unproject = camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
        counter++;

        return unproject;
    }

    public boolean isLeftPressed() {
        return leftPressed;
    }

    public boolean isRightPressed() {
        return rightPressed;
    }

    public boolean isUpPressed() {
        return upPressed;
    }

    public boolean isDownPressed() {
        return downPressed;
    }

    public boolean isSpacePressed() {
        return spacePressed;
    }
}
