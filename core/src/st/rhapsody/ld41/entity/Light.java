package st.rhapsody.ld41.entity;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld41.behaviour.LightBehaviour;
import st.rhapsody.ld41.resource.Sprites;

public class Light {

    private Vector2 position = new Vector2();
    private Color color = new Color(Color.WHITE);
    private float scale = 1.0f;

    private float ttlCounter = 0.0f;
    private float ttl = 0.0f;
    private boolean disposed;

    private float lightOffsetX = 0;
    private float lightOffsetY = 0;

    private LightBehaviour lightBehaviour;

    private Sprite lightSprite = Sprites.light;

    public void tick(double delta){

        if (lightBehaviour != null){
            lightBehaviour.tick(this, delta);
        }

        if (ttl != 0.0f){
            ttlCounter -= delta;

            if (ttlCounter <= 0.0f){
                setDisposed();
            }

        }
    }

    private void setDisposed() {
        this.disposed = true;
    }

    public void render(SpriteBatch lightBatch) {
        lightSprite.setColor(color);
        lightSprite.setScale(scale);
        lightSprite.setOriginBasedPosition(position.x+lightOffsetX, position.y+lightOffsetY);
        lightSprite.draw(lightBatch);
    }

    public Light onPosition(Vector2 position) {
        this.position.set(position);
        return this;
    }

    public Light withColor(Color color) {
        this.color.set(color);
        return this;
    }

    public Light withScale(float scale){
        this.scale = scale;
        return this;
    }

    public Light withTTL(float ttl){
        this.ttl = ttl;
        this.ttlCounter = ttl;
        return this;
    }

    public Light withBehaviour(LightBehaviour lightBehaviour){
        this.lightBehaviour = lightBehaviour;
        return this;
    }

    public boolean isDisposed() {
        return disposed;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public Light withLightOffsetX(float lightOffsetX) {
        this.lightOffsetX = lightOffsetX;
        return this;
    }

    public Light withLightOffsetY(float lightOffsetY) {
        this.lightOffsetY = lightOffsetY;
        return this;
    }

    public Light withSprite(Sprite sprite){
        this.lightSprite = sprite;
        return this;
    }
}
