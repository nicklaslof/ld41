package st.rhapsody.ld41.entity;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Array;
import st.rhapsody.ld41.behaviour.BallBehaviour;
import st.rhapsody.ld41.behaviour.Behaviour;
import st.rhapsody.ld41.behaviour.PlayerBehaviour;
import st.rhapsody.ld41.behaviour.ZombieBehaviour;
import st.rhapsody.ld41.level.Level;
import st.rhapsody.ld41.physic.Box2dBuilder;
import st.rhapsody.ld41.physic.Box2dTemplate;
import st.rhapsody.ld41.resource.Sounds;
import st.rhapsody.ld41.resource.Sprites;
import st.rhapsody.ld41.screen.GameScreen;
import st.rhapsody.ld41.tile.Tile;
import static st.rhapsody.ld41.physic.Box2dConvert.toBox;
import static st.rhapsody.ld41.physic.Box2dConvert.toWorld;
import static st.rhapsody.ld41.physic.Box2dTemplate.PLAYER;
import static st.rhapsody.ld41.physic.Box2dTemplate.SENSOR;

public class Entity {
    private Array<Sprite> sprites = new Array<Sprite>();
    private Array<Sprite> walkSprites = new Array<Sprite>();
    private Array<Sprite> walkUpSprites = new Array<Sprite>();

    private Array<Sprite> waterSprites = new Array<Sprite>();
    private Array<Sprite> waterUpSprites = new Array<Sprite>();

    private Array<Sprite> happySprites = new Array<Sprite>();

    private Array<Sprite> swingSprites = new Array<Sprite>();
    private Array<Sprite> swingUpSprites = new Array<Sprite>();

    private Vector2 position = new Vector2();
    private Behaviour behaviour;
    private float scale = 1.0f;
    private boolean walking = false;
    private boolean walkingUp = false;
    private Sprite spriteToDraw;
    private int walkingFrame = 0;
    private int walkingUpFrame = 0;
    private float timer;
    private float walkAnimationDelay;
    private boolean flippedLeft;
    private Body body;
    private float spriteOffsetX;
    private float spriteOffsetY;
    private boolean disposed;
    private Color color;
    private boolean swingShoot;
    private boolean swing;
    private int swingFrame;
    private boolean animation = true;

    private boolean player = false;

    private Light light;
    private boolean inWater;
    private boolean ball;
    private boolean playOnce;
    private Body sensor;
    private boolean happy;
    private int happyFrame;

    private int health = 5;
    private boolean knockBack;
    private float hitCountdown;
    private boolean enemy = false;
    private float timeToDamage = -1000f;
    private float entityHitCountdown = 0.5f;
    private float splashCounter;
    private boolean inSand;
    private boolean inRuff;
    private float winTuneCounter;
    private boolean playWinTune = false;
    private float sandParticleCounter;

    public Entity addSprite(Sprite sprite){
        sprites.add(sprite);
        spriteToDraw = sprite;
        return this;
    }

    public void render(SpriteBatch spriteBatch) {
        if (spriteToDraw != null){
            //System.out.println(spriteToDraw);
            if (color != null) {
                spriteToDraw.setColor(color);
            }

            if (inWater) {
                if (splashCounter > 0.4f){
                    Sprites.splash6.setPosition(position.x + spriteOffsetX, position.y + spriteOffsetY-2);
                    Sprites.splash6.draw(spriteBatch);

                }else if (splashCounter < 0.2f){
                    Sprites.splash7.setPosition(position.x + spriteOffsetX, position.y + spriteOffsetY-2);
                    Sprites.splash7.draw(spriteBatch);
                }else
                if (splashCounter > 0.2f){
                    Sprites.splash5.setPosition(position.x + spriteOffsetX, position.y + spriteOffsetY-2);
                    Sprites.splash5.draw(spriteBatch);
                }
            }

            spriteToDraw.setScale(scale);
            spriteToDraw.setPosition(position.x+spriteOffsetX, position.y+spriteOffsetY);
            if (flippedLeft){
                spriteToDraw.setFlip(true,false);
            }else{
                spriteToDraw.setFlip(false,false);
            }



            spriteToDraw.draw(spriteBatch);
        }
    }

    public void tick(float delta) {
        //System.out.println(happy);

        Level level = GameScreen.getLevel();
        Tile backgroundTile = level.getBackgroundTile((int) (getPosition().x) / 16, (int) (getPosition().y) / 16);
        if (isPlayer() || isEnemy()) {
            if (backgroundTile.getType().equals(Tile.Type.WATER)){
                this.inWater = true;
            }else{
                this.inWater = false;
            }

            if (backgroundTile.getType().equals(Tile.Type.SAND)){
                this.inSand = true;
            }else{
                this.inSand = false;
            }

            if (backgroundTile.getType().equals(Tile.Type.RUFF)){
                this.inRuff = true;
            }else{
                this.inRuff = false;
            }
        }


        if (health <= 0){
            if (isEnemy()){
                Sounds.zombieDead.play(1.0f);
            }

            if (isPlayer()){
                Level.setPlayerDead();
            }
            setDisposed();
        }

        if (hitCountdown > 0.0f){
            hitCountdown -= delta;
        }

        if (hitCountdown <= 0.0f){
            hitCountdown = 0.0f;
        }

        if (splashCounter > 0.0f){
            splashCounter -= delta;
        }

        if (splashCounter <= 0.0f){
            splashCounter = 0.6f;
        }

        if (timeToDamage > 0.0f){
            timeToDamage -= delta;
        }

        if (timeToDamage < 0.0f){
            timeToDamage = 0.0f;
        }

        if (sandParticleCounter > 0.0f){
            sandParticleCounter -= delta;
        }

        if (sandParticleCounter < 0.0f){
            sandParticleCounter = 0.0f;
        }

        if (isPlayer()) {
            if (winTuneCounter > 0.0f) {
                winTuneCounter -= delta;
            }

            if (winTuneCounter < 0.0f) {
                winTuneCounter = 0.0f;
            }

            if (winTuneCounter == 0.0f && playWinTune) {

                Sounds.win.play(0.4f);
                playWinTune = false;
            }
        }



        if (behaviour != null) {
            behaviour.tick(this, delta);
        }

        if (light != null){
            light.tick(delta);
            light.onPosition(getPosition());
        }

        if (isPlayer() && Level.isPlayerShooting()){
            stop();
        }

        timer += delta;

        if (happy) {
            if (timer > walkAnimationDelay) {
                timer = 0.0f;

                spriteToDraw = happySprites.get(happyFrame);

                happyFrame++;

                if (happyFrame > happySprites.size - 1) {
                    happyFrame = 0;
                }
            }
        }



        if (animation){
            //System.out.println(animation);
            if (timer > walkAnimationDelay) {
                timer = 0.0f;

                if (walking) {
                    if (!inWater) {
                        spriteToDraw = walkSprites.get(walkingFrame);
                    }else{
                        spriteToDraw = waterSprites.get(walkingFrame);
                    }

                    walkingFrame++;

                    if (walkingFrame > walkSprites.size - 1) {
                        if (playOnce){
                            setDisposed();
                        }else {
                            walkingFrame = 0;
                        }
                    }
                }else {

                    if (walkingUp) {
                        if (!inWater) {
                            spriteToDraw = walkUpSprites.get(walkingUpFrame);
                        }else{
                            spriteToDraw = waterUpSprites.get(walkingUpFrame);
                        }

                        walkingUpFrame++;

                        if (walkingUpFrame > walkUpSprites.size - 1) {
                            walkingUpFrame = 0;
                        }
                    }
                }

                if (swing && !walkingUp) {
                    if (swingFrame >= 2) {
                        swingFrame = 0;
                    }
                    spriteToDraw = swingSprites.get(swingFrame);
                    swingFrame++;
                }

                if (swing && walkingUp) {
                    if (swingFrame >= 2) {
                        swingFrame = 0;
                    }
                    spriteToDraw = swingUpSprites.get(swingFrame);
                    swingFrame++;
                }


                if (swingShoot && !walkingUp) {
                    spriteToDraw = swingSprites.get(swingFrame);

                    swingFrame++;

                    if (swingFrame > 3) {
                        spriteToDraw = swingSprites.get(3);
                        swingShoot = false;
                        swing = false;
                    }
                }

                if (swingShoot && walkingUp) {
                    spriteToDraw = swingUpSprites.get(swingFrame);

                    swingFrame++;

                    if (swingFrame > 3) {
                        spriteToDraw = swingUpSprites.get(3);
                        swingShoot = false;
                        swing = false;
                    }
                }



            }


        }


        if (body != null){
            position.set(toWorld(body.getPosition().x), toWorld(body.getPosition().y));
            //System.out.println(position);
        }

        if (sensor != null){
            this.sensor.setTransform(new Vector2(toBox(getPosition().x), toBox(getPosition().y-2)),0);
            //System.out.println(position);
        }

    }

    public Body getSensor() {
        return sensor;
    }

    public Entity onPosition(Vector2 vector2) {
        if (body != null){
            body.setTransform(toBox(vector2.x + spriteToDraw.getWidth()/2), toBox(vector2.y + spriteToDraw.getHeight()/2),0);
        }else {
            position.set(vector2);
        }
        return this;
    }

    public Vector2 getPosition() {
        if (body != null){
            position.set(toWorld(body.getPosition().x), toWorld(body.getPosition().y));
        }

        return position;

    }

    public Entity withBehaviour(Behaviour behaviour) {
        this.behaviour = behaviour;
        return this;
    }

    public Entity withScale(float scale) {
        //System.out.println(scale);
        this.scale = scale;
        return this;
    }

    public Entity withColor(Color color){
        this.color = color;
        return this;
    }

    public void scale(float scale){
        this.scale += scale;
    }

    public Entity addWalkSprite(Sprite sprite) {
        this.walkSprites.add(sprite);
        return this;
    }

    public Entity addWaterSprite(Sprite sprite) {
        this.waterSprites.add(sprite);
        return this;
    }

    public Entity addHappySprite(Sprite sprite) {
        this.happySprites.add(sprite);
        return this;
    }

    public Entity addWalkUpSprite(Sprite sprite) {
        this.walkUpSprites.add(sprite);
        return this;
    }

    public Entity addWaterUpSprite(Sprite sprite) {
        this.waterUpSprites.add(sprite);
        return this;
    }

    public Entity withWalkAnimationDelay(float walkAnimationDelay) {
        this.walkAnimationDelay = walkAnimationDelay;
        return this;
    }

    public void setWalking(boolean walking) {
        if (!this.walking && walking){
            timer = walkAnimationDelay;
        }
        this.walking = walking;

    }

    public void setWalkingUp(boolean walking) {
        if (!this.walkingUp && walking){
            timer = walkAnimationDelay;
        }
        this.walkingUp = walking;
    }

    public void setSwing(boolean swing){
        this.swing = swing;

    }
    public void setSwingShoot(boolean swing){
        this.swingShoot = swing;
        if (swing) {
            this.swingFrame = 2;
        }
    }

    public void setHappy(boolean b){
        this.happy = b;
        if (b) {
            winTuneCounter = 0.4f;
            playWinTune = true;
        }
    }


    public void flipLeft() {
        this.flippedLeft = true;
    }
    public void flipRight() {
        this.flippedLeft = false;
    }

    public void setDisposed() {
        this.disposed = true;
    }

    public boolean isDisposed() {
        return disposed;
    }

    public Entity withBox2dTemplate(Box2dTemplate box2dTemplate) {

        this.body = Box2dBuilder.build(box2dTemplate);
        this.body.setUserData(this);

        return this;
    }

    public Entity withSpriteOffsetX(float x){
        this.spriteOffsetX = x;
        return this;
    }

    public Entity withSpriteOffsetY(float y){
        this.spriteOffsetY = y;
        return this;
    }

    public void transform(float x, float y) {
        if (body != null){
            if (inWater){
                x /= 16;
                y /= 16;
            }

            if (inSand || inRuff){
                x /= 8;
                y /= 8;
            }

            if (inSand){
                if (sandParticleCounter == 0.0f) {
                    Level.spawnParticles(new Vector2(getPosition().x - 2, getPosition().y - 5), 2.0f, 10, new Color(0xfff392ff));
                    sandParticleCounter = 0.5f;
                }
            }

            /*if (inWater){
                if (sandParticleCounter == 0.0f) {
                    Level.spawnParticles(new Vector2(getPosition().x - 2, getPosition().y - 5), 2.0f, 10, new Color(0x92d3ffff));
                    sandParticleCounter = 0.5f;
                }
            }*/

            body.applyForceToCenter(toBox(x), toBox(y),true);
            if (body.getLinearVelocity().x > 0.15f){
                body.setLinearVelocity(0.15f,body.getLinearVelocity().y);
            }

            if (body.getLinearVelocity().x < -0.15f){
                body.setLinearVelocity(-0.15f,body.getLinearVelocity().y);
            }

            if (body.getLinearVelocity().y > 0.15f){
                body.setLinearVelocity(body.getLinearVelocity().x, 0.15f);
            }

            if (body.getLinearVelocity().y < -0.15f){
                body.setLinearVelocity(body.getLinearVelocity().x, -0.15f);
            }

        }else{
            position.add(x, y);
        }
    }

    public void stop() {
        if (body != null){
            body.setLinearVelocity(0,0);
        }
    }

    public Body getBody() {
        return body;
    }

    public void collidedWithFlag() {
        //System.out.println("COLLIDED!");
        if (behaviour instanceof BallBehaviour){
            BallBehaviour ballBehaviour = (BallBehaviour) this.behaviour;
            //if (ballBehaviour.hasLanded()){
                setDisposed();
                Sounds.ballincup.play(1.0f);
            Level.setPlayerHappy(true);
            Level.healPlayer();

          //  }
        }
    }

    public Entity addSwingSprite(Sprite sprite) {
        this.swingSprites.add(sprite);
        return this;
    }

    public Entity addSwingUpSprite(Sprite sprite) {
        this.swingUpSprites.add(sprite);
        return this;
    }

    public void stopAllAnimations() {
        animation = false;
    }

    public void enableAnimations() {
        animation = true;
    }

    public Entity  withHealth(int health){
        this.health = health;
        return this;
    }

    public int getHealth() {
        return health;
    }

    public void hit(int ammount){
        if (hitCountdown == 0.0f) {
            this.health -= ammount;
            this.hitCountdown = entityHitCountdown;
            if (isPlayer()) {
                Level.spawnParticles(getPosition(), 5, 80, new Color(0xff0000ff));
            }

            if (isEnemy()){
                Level.spawnParticles(getPosition(), 5, 80, new Color(0x0000ffff));
            }

            if (isPlayer()) {
                Sounds.zombieBite.play(0.5f);
            }
        }
    }

    public void heal(){
        if (isPlayer()){
            //if (health < 5){
                health++;
           // }
        }
    }

    public Entity withEntityHitCountdown(float n){
        this.entityHitCountdown = n;
        this.hitCountdown = n;
        return this;
    }

    public Entity withLight(Light light){
        this.light = light;
        return this;
    }

    public void renderLight(SpriteBatch lightSpriteBatch) {
        if (light != null){
            light.render(lightSpriteBatch);
        }
    }

    public void setIsPlayer() {
        this.player = true;
    }

    public boolean isPlayer() {
        return player;
    }

    public void setIsBall() {
        this.ball = true;
    }

    public boolean isBall() {
        return this.ball;
    }

    public void setIsKnockback(){
        this.knockBack = true;
    }

    public boolean isKnockBack() {
        return knockBack;
    }

    public void collideWithTile(Tile tile) {
        if (isPlayer() && tile.getType() == Tile.Type.WATER){
            this.inWater = true;
        }

        if (isBall() && tile.getType() == Tile.Type.FLAG){
            collidedWithFlag();
        }

        if (isBall() && tile.getType() == Tile.Type.WATER){
            if (behaviour instanceof BallBehaviour){
                BallBehaviour ballBehaviour = (BallBehaviour) this.behaviour;
                if (ballBehaviour.hasLanded()){
                    setDisposed();
                    Sounds.splash.play(1.0f);
                    Level.setPlayerShooting(true);
                    Level.spawnSplash(getPosition());
                    Level.setBallInWater();
                }
            }
        }


    }

    public Entity playOnce() {
        this.playOnce = true;
        return this;


    }

    public void addSensor() {
        Box2dTemplate box2dTemplate = new Box2dTemplate()
                .asSensor()
                .asType(Box2dTemplate.Type.CIRCLE)
                .filterIsA(SENSOR)
                .filterCollidesWith(PLAYER)
                .withRadius(5);


        this.sensor = Box2dBuilder.build(box2dTemplate);
        this.sensor.setUserData(this);


    }

    public void collideWithEntity(Entity entity) {
        if (!isDisposed() && !entity.isDisposed()) {
            if (entity.isEnemy() && isEnemy()){
                return;
            }
            if (isBall() && entity.isPlayer()) {
                Level.setPlayerShooting(true);
                entity.stop();
                //entity.stopAllAnimations();
                setDisposed();
            }

            if (isBall() && entity.isEnemy()) {
                if (behaviour instanceof BallBehaviour){
                    float ballSpeed = ((BallBehaviour)behaviour).getSpeed();
                    if (ballSpeed != 0){
                        entity.hit(1);
                    }
                }

            }

            if (entity.isKnockBack() && hitCountdown == 0.0f) {
                hit(1);
            }
        }

        if (isPlayer() && entity.isEnemy() && !isHappy()) {
                if (MathUtils.random(0,1) <1) {
                    hit(1);


                }
        }
    }

    public boolean isHappy() {
        return happy;
    }

    public boolean isSwingShooting() {
        return swingShoot;
    }

    public boolean isEnemy() {
        return enemy;
    }

    public void setEnemy(boolean enemy) {
        this.enemy = enemy;
    }

    public boolean isFlippedLeft() {
        return flippedLeft;
    }

    public boolean isFlippedRight() {
        return !flippedLeft;
    }

    public boolean isBallMoving() {
        if (isBall() && behaviour instanceof BallBehaviour){
            boolean moving = ((BallBehaviour) behaviour).isBallMoving();
            return moving;
        }

        return false;
    }

    public float getScale() {
        return scale;
    }

    public boolean isInWater() {
        return inWater;
    }

    public boolean isInSand() {
        return inSand;
    }
}
