package st.rhapsody.ld41.entity;


import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld41.behaviour.ParticleBehaviour;

public class Particle {
    private Vector2 position;
    private ParticleBehaviour behaviour;
    private Sprite sprite;
    private boolean disposed;

    protected double ttl = 1.0f;

    public Particle withBehaviour(ParticleBehaviour behaviour) {
        this.behaviour = behaviour;
        return this;
    }

    public Particle onPosition(Vector2 position) {
        this.position = position.cpy();
        return this;
    }

    public Particle withTTL(double ttl){
        this.ttl = ttl;
        return this;
    }

    public Particle withSprite(Sprite sprite) {
        this.sprite = sprite;
        return this;
    }

    public float getPosX(){
        return position.x;
    }

    public float getPosY(){
        return position.y;
    }

    public void tick(double delta){
        this.behaviour.tick(delta, this);
        this.ttl -= delta;

        if (ttl <= 0){
            setDisposed();
        }
    }

    public void render(SpriteBatch spriteBatch){
        sprite.setColor(behaviour.getColor());
        sprite.setPosition(position.x, position.y);
        sprite.setScale(behaviour.getScale());
        sprite.draw(spriteBatch);
    }

    public void setDisposed() {
        this.disposed = true;
    }

    public boolean isDisposed() {
        return disposed;
    }

    public void transform(float x, float y) {
        this.position.add(x, y);
    }
}