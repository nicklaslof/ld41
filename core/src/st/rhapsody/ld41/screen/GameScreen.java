package st.rhapsody.ld41.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.utils.Align;
import st.rhapsody.ld41.LD41;
import st.rhapsody.ld41.input.InputController;
import st.rhapsody.ld41.level.Level;
import st.rhapsody.ld41.physic.Box2dConvert;
import st.rhapsody.ld41.resource.Sprites;
import st.rhapsody.ld41.resource.Textures;

public class GameScreen implements Screen{
    public static Vector2 mousePositionWorldSpace = new Vector2();
    private OrthographicCamera spriteCamera;
    private SpriteBatch spriteBatch;
    private static Level level;
    public static InputController inputController;
    private OrthographicCamera uiCamera;
    private SpriteBatch uiBatch;
    private Box2DDebugRenderer debugRenderer;
    private Matrix4 debugCameraMatrix;

    private FrameBuffer lightBuffer;
    private TextureRegion lightBufferRegion;
    private SpriteBatch lightSpriteBatch;
    private SpriteBatch finalLight;

    private float playerDeadTimeout = 2.0f;


    @Override
    public void show() {
        inputController = new InputController();
        Gdx.input.setInputProcessor(inputController);

        spriteCamera = new OrthographicCamera(800,600);
        spriteCamera.zoom = 0.5f;
        spriteCamera.position.set(0,0,0);
        spriteCamera.update();


        uiCamera = new OrthographicCamera(800,600);
        uiCamera.position.set(0,0,0);
        uiCamera.update();
        spriteBatch = new SpriteBatch();
        uiBatch = new SpriteBatch();

        level = new Level(32, 64);
        level.setCourse(1, Textures.level1);


        level.updateCamera(spriteCamera);

        Gdx.input.setCursorCatched(true);
        Gdx.input.setCursorPosition(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);

        debugRenderer = new Box2DDebugRenderer();

        lightBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, 800,600, false);
        lightBuffer.getColorBufferTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        finalLight = new SpriteBatch();
        lightSpriteBatch = new SpriteBatch();
    }

    @Override
    public void render(float delta) {
        /*if (Gdx.input.isKeyJustPressed(Input.Keys.R)){
            level = new Level(32, 64);
            level.setCourse(1, Textures.level1);
        }*/

        if (IntroScreen.introMusicPlaying()) {
            IntroScreen.fadeOutIntro(delta);
        }

        if (level.isPlayerDead()){
            playerDeadTimeout -=delta;
        }

        if (playerDeadTimeout < 0.0f && level.isPlayerDead() && inputController.isSpacePressed()){
            /*level = new Level(32, 64);
            level.setCourse(1, Textures.level1);*/
            LD41.switchToIntroScreen();
            playerDeadTimeout = 2.0f;
        }

        if (level.isPlayerFinished()){
            playerDeadTimeout -=delta;
        }

        if (playerDeadTimeout < 0.0f && level.isPlayerFinished() && inputController.isSpacePressed()){
            /*level = new Level(32, 64);
            level.setCourse(1, Textures.level1);*/
            LD41.switchToIntroScreen();
            playerDeadTimeout = 2.0f;
        }



        Vector3 projectedPosition = inputController.getProjectedPosition(spriteCamera);
        mousePositionWorldSpace.set(projectedPosition.x, projectedPosition.y);

        if (!level.isPlayerDead() && !level.isPlayerFinished()) {
            level.tick(delta);
        }

        level.updateCamera(spriteCamera);


        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        spriteCamera.update();

        spriteBatch.setProjectionMatrix(spriteCamera.combined);
        spriteBatch.enableBlending();
        spriteBatch.begin();

        level.render(spriteBatch);
        spriteBatch.end();



        renderLight();


        mergeLights();


        spriteBatch.begin();
        inputController.renderCrosshair(spriteBatch, spriteCamera);
        spriteBatch.end();

        drawUI();

        //renderDebug();



    }

    private void drawUI() {
        uiBatch.setProjectionMatrix(uiCamera.combined);
        uiBatch.begin();

        Sprites.levelMap.setScale(3.0f);
        Sprites.levelMap.setAlpha(0.4f);
        Sprites.levelMap.setPosition(320, -130);

        Sprites.levelMap.draw(uiBatch);

        level.renderBackSwing(uiBatch);

        int health = level.getPlayer().getHealth();
        int score = level.getScore();
        int par = level.getPar();
        int shots = level.getPlayerShots();

        if (!level.isPlayerFinished()) {
            FontDrawer.drawTextAt(uiBatch, "HEALTH " + health, -380, 280, false, Align.left);
            FontDrawer.drawTextAt(uiBatch, "SCORE " + score, 250, 280, false, Align.left);

            FontDrawer.drawTextAt(uiBatch, "PAR " + par, 295, -200, false, Align.left);
            FontDrawer.drawTextAt(uiBatch, "SHOTS " + shots, 270, -220, false, Align.left);
        }

        if (!level.isPlayerDead() && !level.isPlayerFinished()) {
            level.renderCourseName(uiBatch);
        }
        level.renderBallInWater(uiBatch);


        if (level.isPlayerDead()){
            FontDrawer.drawTextAt(uiBatch,"GAME OVER", -80,0,false, Align.left);
            FontDrawer.drawTextAt(uiBatch, "PRESS SPACE TO PLAY AGAIN ", -200, -40, false, Align.left);
        }


        if (level.isPlayerFinished()){
            FontDrawer.drawTextAt(uiBatch,"CONGRATUALTIONS!", -120,90,false, Align.left);
            FontDrawer.drawTextAt(uiBatch, "YOU FINISHED ALL COURSES WITH A SCORE OF "+level.getScore(), -335, 50, false, Align.left);
        }


        uiBatch.end();
    }

    private void renderLight() {
        lightBuffer.begin();
        Gdx.gl.glClearColor(0.20f,0.20f,0.40f,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        lightSpriteBatch.enableBlending();
        lightSpriteBatch.begin();
        lightSpriteBatch.setProjectionMatrix(spriteCamera.combined);

        lightSpriteBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
        level.renderLights(lightSpriteBatch);

        lightSpriteBatch.end();
        lightBuffer.end();
    }

    private void mergeLights() {
        finalLight.enableBlending();
        finalLight.setBlendFunction(GL20.GL_DST_COLOR, GL20.GL_ZERO);
        finalLight.begin();
        finalLight.setProjectionMatrix(lightSpriteBatch.getProjectionMatrix().idt());
        finalLight.draw(lightBuffer.getColorBufferTexture(), -1, 1, 2, -2);
        finalLight.end();
        finalLight.disableBlending();
    }

    private void renderDebug(){
        debugCameraMatrix=new Matrix4(spriteCamera.combined);

        debugCameraMatrix.translate(spriteCamera.position.x/1024, spriteCamera.position.y/1024, 0f);
        debugCameraMatrix.scale(Box2dConvert.BOX_TO_WORLD, Box2dConvert.BOX_TO_WORLD, 0);


        level.debugRender(debugRenderer, debugCameraMatrix);
    }


    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public static Level getLevel() {
        return level;
    }
}
