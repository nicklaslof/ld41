package st.rhapsody.ld41.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class FontDrawer {

    private static GlyphLayout layout;
    private static BitmapFont font;

    public FontDrawer() {
        layout = new GlyphLayout();
        font = new BitmapFont(Gdx.files.internal("ld41-font.fnt"), Gdx.files.internal("ld41-font.png"), false);
    }

    public static void drawTextAt(SpriteBatch spriteBatch, String text, int x, int y, boolean wrap, int align){
        drawTextAt(spriteBatch, text, x, y, wrap, align, 1.0f);
    }

    public static void drawTextAt(SpriteBatch spriteBatch, String text, int x, int y, boolean wrap, int align, float scale){

        text = text.toUpperCase();
        font.setUseIntegerPositions(false);
        font.getData().setLineHeight(20f);
        font.getData().setScale(scale);

        if (wrap) {
            GlyphLayout draw = font.draw(spriteBatch, text, x, y, 450, align, true);

            font.draw(spriteBatch, draw, x, y);
        }else{
            layout.setText(font, text);
            font.draw(spriteBatch,layout,x,y);
        }

    }

    public void dispose() {
        font.dispose();
    }
}