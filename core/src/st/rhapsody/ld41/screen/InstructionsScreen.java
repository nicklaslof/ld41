package st.rhapsody.ld41.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;
import st.rhapsody.ld41.LD41;
import st.rhapsody.ld41.resource.Sprites;

public class InstructionsScreen implements Screen {
    private OrthographicCamera spriteCamera;
    private SpriteBatch spriteBatch;
    private float timeoutCounter = 3.0f;

    @Override
    public void show() {
        spriteCamera = new OrthographicCamera(800,600);
        spriteCamera.zoom = 0.5f;
        spriteCamera.position.set(0,0,0);
        spriteCamera.update();

        spriteBatch = new SpriteBatch();

        Gdx.input.setCursorCatched(false);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0.0f, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        timeoutCounter -= delta;
        //System.out.println(timeoutCounter);
        spriteBatch.setProjectionMatrix(spriteCamera.combined);

        spriteBatch.begin();

        Sprites.logo.setScale(2.0f);
        Sprites.logo.setPosition(-45, 40);
        Sprites.logo.draw(spriteBatch);


        FontDrawer.drawTextAt(spriteBatch,"our hero wants to play some golf in the evening", -155,15,false, Align.left, 0.4f);
        FontDrawer.drawTextAt(spriteBatch,"what could possibly go wrong?", -85,0,false, Align.left, 0.4f);



        FontDrawer.drawTextAt(spriteBatch,"instructions", -55,-30,false, Align.left, 0.6f);

        FontDrawer.drawTextAt(spriteBatch,"aim with the mouse and watch the strength meter", -155,-50,false, Align.left, 0.4f);

        FontDrawer.drawTextAt(spriteBatch,"click the mouse to shoot the ball", -105,-70,false, Align.left, 0.4f);

        FontDrawer.drawTextAt(spriteBatch,"get the ball to the flag", -80,-90,false, Align.left, 0.4f);

        FontDrawer.drawTextAt(spriteBatch,"move around with ASDW", -70,-110,false, Align.left, 0.4f);

        FontDrawer.drawTextAt(spriteBatch,"oh.. also watch out for some zombies!", -125,-140,false, Align.left, 0.4f);

        spriteBatch.end();


        if (timeoutCounter < 0.0f && (Gdx.input.isKeyJustPressed(Input.Keys.SPACE) || Gdx.input.isTouched())){
            LD41.switchToGameScreen();
        }

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
