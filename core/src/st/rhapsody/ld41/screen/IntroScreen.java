package st.rhapsody.ld41.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;
import st.rhapsody.ld41.LD41;
import st.rhapsody.ld41.resource.Sounds;
import st.rhapsody.ld41.resource.Sprites;

public class IntroScreen implements Screen{
    private OrthographicCamera spriteCamera;
    private SpriteBatch spriteBatch;
    private float counter = 0.1f;
    private Sprite flagSprite;

    private float timeoutCounter = 3.0f;

    private Sprite[] flag = new Sprite[]{Sprites.flag1, Sprites.flag2, Sprites.flag3, Sprites.flag4, Sprites.flag5};
    private int animationFrame;
    private static long introSound;
    private static float volume = 0.5f;


    public static void fadeOutIntro(float delta){
        volume -= delta/20.0f;
        Sounds.introMusic.setVolume(introSound, volume);

        if (volume <= 0.0f){
            Sounds.introMusic.stop(introSound);
        }
    }

    public static boolean introMusicPlaying(){
        return volume > 0.0f;
    }

    @Override
    public void show() {
        spriteCamera = new OrthographicCamera(800,600);
        spriteCamera.zoom = 0.5f;
        spriteCamera.position.set(0,0,0);
        spriteCamera.update();

        spriteBatch = new SpriteBatch();

        Gdx.input.setCursorCatched(false);
        introSound = Sounds.introMusic.loop(0.5f);
        volume = 0.5f;
        //Gdx.input.setCursorPosition(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0, 0.0f, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        timeoutCounter -= delta;

        /*if (flagSprite == null){
            flagSprite = flag[0];
        }

        if (counter > 0.0f){
            counter -= delta;
        }

        if (counter < 0.0f){
            counter = 0.1f;
            animationFrame++;

            if (animationFrame > flag.length-1){
                animationFrame = 0;
            }

            flagSprite = flag[animationFrame];
            //System.out.println(flagSprite + " "+animationFrame);
        }




        Sprites.char_walk1.setPosition(20,44);

        Sprites.char_walk1.setScale(2.0f);
        Sprites.char_walk1.setFlip(true,false);
        Sprites.char_walk1.draw(spriteBatch);

        flagSprite.setPosition(-20,40);
        flagSprite.draw(spriteBatch);
        flagSprite.setScale(2.0f);*/

        spriteBatch.setProjectionMatrix(spriteCamera.combined);

        spriteBatch.begin();
        Sprites.logo.setScale(2.0f);

        Sprites.logo.setPosition(-45, 40);
        Sprites.logo.draw(spriteBatch);


        FontDrawer.drawTextAt(spriteBatch,"a game for ludum dare 41", -95,-20,false, Align.left, 0.5f);
        FontDrawer.drawTextAt(spriteBatch,"by nicklas lof and mathias lindfeldt", -145,-40,false, Align.left, 0.5f);

        spriteBatch.end();

        if (timeoutCounter < 0.0f && (Gdx.input.isKeyJustPressed(Input.Keys.SPACE) || Gdx.input.isTouched())){
            LD41.switchToInstructionScreen();
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
