package st.rhapsody.ld41.resource;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class Textures {
    private final Texture texture;
    public static Texture level1;
    public static Texture level2;
    public static Texture level3;
    public static Texture level4;
    public static Texture level5;
    public static Texture level6;
    public static Texture level7;
    public static Texture level8;
    public static Texture level9;
    public static TextureAtlas textureAtlas;

    public Textures() {

        texture = new Texture(Gdx.files.internal("atlas.png"));
        level1 = new Texture(Gdx.files.internal("level1.png"));
        level2 = new Texture(Gdx.files.internal("level2.png"));
        level3 = new Texture(Gdx.files.internal("level3.png"));
        level4 = new Texture(Gdx.files.internal("level4.png"));
        level5 = new Texture(Gdx.files.internal("level5.png"));
        level6 = new Texture(Gdx.files.internal("level6.png"));
        level7 = new Texture(Gdx.files.internal("level7.png"));
        level8 = new Texture(Gdx.files.internal("level8.png"));
        level9 = new Texture(Gdx.files.internal("level9.png"));
        texture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);

        textureAtlas = new TextureAtlas();

        textureAtlas.addRegion("char_walk1",texture,0,0,16,16);
        textureAtlas.addRegion("char_walk2",texture,16,0,16,16);
        textureAtlas.addRegion("char_walk3",texture,32,0,16,16);
        textureAtlas.addRegion("char_walk4",texture,48,0,16,16);

        textureAtlas.addRegion("char_walkup1",texture,64,0,16,16);
        textureAtlas.addRegion("char_walkup2",texture,80,0,16,16);
        textureAtlas.addRegion("char_walkup3",texture,96,0,16,16);
        textureAtlas.addRegion("char_walkup4",texture,112,0,16,16);

        textureAtlas.addRegion("char_happy1",texture,128,0,16,16);
        textureAtlas.addRegion("char_happy2",texture,144,0,16,16);

        textureAtlas.addRegion("char_swing1",texture,160,0,16,16);
        textureAtlas.addRegion("char_swing2",texture,176,0,16,16);
        textureAtlas.addRegion("char_swing3",texture,192,0,16,16);
        textureAtlas.addRegion("char_swing4",texture,208,0,16,16);

        textureAtlas.addRegion("char_swingup1",texture,224,0,16,16);
        textureAtlas.addRegion("char_swingup2",texture,240,0,16,16);
        textureAtlas.addRegion("char_swingup3",texture,256,0,16,16);
        textureAtlas.addRegion("char_swingup4",texture,272,0,16,16);



        textureAtlas.addRegion("char_water1",texture,0,172,16,16);
        textureAtlas.addRegion("char_water2",texture,16,172,16,16);
        textureAtlas.addRegion("char_water3",texture,32,172,16,16);
        textureAtlas.addRegion("char_water4",texture,48,172,16,16);

        textureAtlas.addRegion("char_waterup1",texture,64,172,16,16);
        textureAtlas.addRegion("char_waterup2",texture,80,172,16,16);
        textureAtlas.addRegion("char_waterup3",texture,96,172,16,16);
        textureAtlas.addRegion("char_waterup4",texture,112,172,16,16);


        textureAtlas.addRegion("zombie_walk1",texture,0,140,16,16);
        textureAtlas.addRegion("zombie_walk2",texture,16,140,16,16);
        textureAtlas.addRegion("zombie_walk3",texture,32,140,16,16);
        textureAtlas.addRegion("zombie_walk4",texture,48,140,16,16);

        textureAtlas.addRegion("zombie_walkup1",texture,64,140,16,16);
        textureAtlas.addRegion("zombie_walkup2",texture,80,140,16,16);
        textureAtlas.addRegion("zombie_walkup3",texture,96,140,16,16);
        textureAtlas.addRegion("zombie_walkup4",texture,112,140,16,16);

        textureAtlas.addRegion("zombie_water1",texture,0,244,16,16);
        textureAtlas.addRegion("zombie_water2",texture,16,244,16,16);
        textureAtlas.addRegion("zombie_water3",texture,32,244,16,16);
        textureAtlas.addRegion("zombie_water4",texture,48,244,16,16);

        textureAtlas.addRegion("zombie_waterup1",texture,64,244,16,16);
        textureAtlas.addRegion("zombie_waterup2",texture,80,244,16,16);
        textureAtlas.addRegion("zombie_waterup3",texture,96,244,16,16);
        textureAtlas.addRegion("zombie_waterup4",texture,112,244,16,16);



        textureAtlas.addRegion("grass", texture, 16,48,16,16);
        textureAtlas.addRegion("green", texture, 64,48,16,16);
        textureAtlas.addRegion("sand", texture, 32,80,16,16);
        textureAtlas.addRegion("stone", texture, 256,32,32,32);
        textureAtlas.addRegion("ruff", texture, 64,80,16,16);
        textureAtlas.addRegion("water", texture, 0,80,16,16);
        textureAtlas.addRegion("water2", texture, 92,80,16,16);
        textureAtlas.addRegion("water3", texture, 108,80,16,16);

        textureAtlas.addRegion("spawner", texture, 0,320,16,16);

        textureAtlas.addRegion("palmtree1", texture, 0, 192, 48, 48);
        textureAtlas.addRegion("palmtree2", texture, 48, 192, 48, 48);


        textureAtlas.addRegion("flag1", texture, 0, 272, 16, 16);
        textureAtlas.addRegion("flag2", texture, 48, 272, 16, 16);
        textureAtlas.addRegion("flag3", texture, 96, 272, 16, 16);
        textureAtlas.addRegion("flag4", texture, 144, 272, 16, 16);
        textureAtlas.addRegion("flag5", texture, 192, 272, 16, 16);



        textureAtlas.addRegion("splash1", texture, 0, 110, 16, 16);
        textureAtlas.addRegion("splash2", texture, 16, 110, 16, 16);
        textureAtlas.addRegion("splash3", texture, 32, 110, 16, 16);
        textureAtlas.addRegion("splash4", texture, 48, 110, 16, 16);
        textureAtlas.addRegion("splash5", texture, 64, 110, 16, 16);
        textureAtlas.addRegion("splash6", texture, 80, 110, 16, 16);
        textureAtlas.addRegion("splash7", texture, 96, 110, 16, 16);
        textureAtlas.addRegion("splash8", texture, 112, 110, 16, 16);




        textureAtlas.addRegion("ball", texture,0,24, 4, 4);
        textureAtlas.addRegion("teeoff", texture,0,292, 14, 10);
        textureAtlas.addRegion("crosshair", texture, 6,24, 5, 5);
        textureAtlas.addRegion("particle", texture, 32, 24, 1, 1);

        textureAtlas.addRegion("bar", texture, 16,24, 5, 1);

        textureAtlas.addRegion("logo", texture, 128,32, 96, 48);




        textureAtlas.addRegion("light", texture, 256,256,256,256);




    }
}
