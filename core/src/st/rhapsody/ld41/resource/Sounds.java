package st.rhapsody.ld41.resource;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

public class Sounds {


    public static Sound swingGrass;
    public static Sound swingSand;
    public static Sound swing;
    public static Sound landingBall;
    public static Sound zombiehit;
    public static Sound zombieDead;
    public static Sound zombieGrunt;
    public static Sound zombieBite;
    public static Sound splash;
    public static Sound ballincup;
    public static Sound introMusic;
    public static Sound win;



    public Sounds() {
        swingGrass = Gdx.audio.newSound(Gdx.files.internal("swinggrass.mp3"));
        swingSand = Gdx.audio.newSound(Gdx.files.internal("swingsand.mp3"));
        landingBall = Gdx.audio.newSound(Gdx.files.internal("landingball.mp3"));
        zombiehit = Gdx.audio.newSound(Gdx.files.internal("zombiehit.mp3"));
        zombieDead = Gdx.audio.newSound(Gdx.files.internal("zombiedead.mp3"));
        zombieBite = Gdx.audio.newSound(Gdx.files.internal("zombiebite.mp3"));
        ballincup = Gdx.audio.newSound(Gdx.files.internal("ballincup.mp3"));
        zombieGrunt = Gdx.audio.newSound(Gdx.files.internal("zombiegrunt1.mp3"));
        splash = Gdx.audio.newSound(Gdx.files.internal("splash.mp3"));
        swing = Gdx.audio.newSound(Gdx.files.internal("swing.mp3"));
        introMusic = Gdx.audio.newSound(Gdx.files.internal("ld41-intro.mp3"));
        win = Gdx.audio.newSound(Gdx.files.internal("ld36win.mp3"));

    }
}
