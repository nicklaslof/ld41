package st.rhapsody.ld41.resource;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class Sprites {

    public static Sprite char_walk1;
    public static Sprite char_walk2;
    public static Sprite char_walk3;
    public static Sprite char_walk4;

    public static Sprite char_walkup1;
    public static Sprite char_walkup2;
    public static Sprite char_walkup3;
    public static Sprite char_walkup4;

    public static Sprite char_water1;
    public static Sprite char_water2;
    public static Sprite char_water3;
    public static Sprite char_water4;

    public static Sprite char_waterup1;
    public static Sprite char_waterup2;
    public static Sprite char_waterup3;
    public static Sprite char_waterup4;

    public static Sprite char_happy1;
    public static Sprite char_happy2;

    public static Sprite char_swing1;
    public static Sprite char_swing2;
    public static Sprite char_swing3;
    public static Sprite char_swing4;

    public static Sprite char_swingup1;
    public static Sprite char_swingup2;
    public static Sprite char_swingup3;
    public static Sprite char_swingup4;



    public static Sprite zombie_walk1;
    public static Sprite zombie_walk2;
    public static Sprite zombie_walk3;
    public static Sprite zombie_walk4;

    public static Sprite zombie_walkup1;
    public static Sprite zombie_walkup2;
    public static Sprite zombie_walkup3;
    public static Sprite zombie_walkup4;

    public static Sprite zombie_water1;
    public static Sprite zombie_water2;
    public static Sprite zombie_water3;
    public static Sprite zombie_water4;

    public static Sprite zombie_waterup1;
    public static Sprite zombie_waterup2;
    public static Sprite zombie_waterup3;
    public static Sprite zombie_waterup4;



    public static Sprite splash1;
    public static Sprite splash2;
    public static Sprite splash3;
    public static Sprite splash4;
    public static Sprite splash5;
    public static Sprite splash6;
    public static Sprite splash7;
    public static Sprite splash8;


    public static Sprite grass;
    public static Sprite sand;
    public static Sprite ruff;
    public static Sprite water;
    public static Sprite water2;
    public static Sprite water3;

    public static Sprite stone;

    public static Sprite spawner;

    public static Sprite palmtree1;
    public static Sprite palmtree2;

    public static Sprite flag1;
    public static Sprite flag2;
    public static Sprite flag3;
    public static Sprite flag4;
    public static Sprite flag5;


    public static Sprite ball;
    public static Sprite teeoff;
    public static Sprite crosshair;
    public static Sprite bar;
    public static Sprite particle;
    public static Sprite green;
    public static Sprite light;
    public static Sprite levelMap;

    public static Sprite logo;

    public Sprites() {

        char_walk1 = Textures.textureAtlas.createSprite("char_walk1");
        char_walk2 = Textures.textureAtlas.createSprite("char_walk2");
        char_walk3 = Textures.textureAtlas.createSprite("char_walk3");
        char_walk4 = Textures.textureAtlas.createSprite("char_walk4");

        char_walkup1 = Textures.textureAtlas.createSprite("char_walkup1");
        char_walkup2 = Textures.textureAtlas.createSprite("char_walkup2");
        char_walkup3 = Textures.textureAtlas.createSprite("char_walkup3");
        char_walkup4 = Textures.textureAtlas.createSprite("char_walkup4");

        char_happy1 = Textures.textureAtlas.createSprite("char_happy1");
        char_happy2 = Textures.textureAtlas.createSprite("char_happy2");

        char_swing1 = Textures.textureAtlas.createSprite("char_swing1");
        char_swing2 = Textures.textureAtlas.createSprite("char_swing2");
        char_swing3 = Textures.textureAtlas.createSprite("char_swing3");
        char_swing4 = Textures.textureAtlas.createSprite("char_swing4");

        char_swingup1 = Textures.textureAtlas.createSprite("char_swingup1");
        char_swingup2 = Textures.textureAtlas.createSprite("char_swingup2");
        char_swingup3 = Textures.textureAtlas.createSprite("char_swingup3");
        char_swingup4 = Textures.textureAtlas.createSprite("char_swingup4");


        char_water1 = Textures.textureAtlas.createSprite("char_water1");
        char_water2 = Textures.textureAtlas.createSprite("char_water2");
        char_water3 = Textures.textureAtlas.createSprite("char_water3");
        char_water4 = Textures.textureAtlas.createSprite("char_water4");

        char_waterup1 = Textures.textureAtlas.createSprite("char_waterup1");
        char_waterup2 = Textures.textureAtlas.createSprite("char_waterup2");
        char_waterup3 = Textures.textureAtlas.createSprite("char_waterup3");
        char_waterup4 = Textures.textureAtlas.createSprite("char_waterup4");



        zombie_walk1 = Textures.textureAtlas.createSprite("zombie_walk1");
        zombie_walk2 = Textures.textureAtlas.createSprite("zombie_walk2");
        zombie_walk3 = Textures.textureAtlas.createSprite("zombie_walk3");
        zombie_walk4 = Textures.textureAtlas.createSprite("zombie_walk4");

        zombie_walkup1 = Textures.textureAtlas.createSprite("zombie_walkup1");
        zombie_walkup2 = Textures.textureAtlas.createSprite("zombie_walkup2");
        zombie_walkup3 = Textures.textureAtlas.createSprite("zombie_walkup3");
        zombie_walkup4 = Textures.textureAtlas.createSprite("zombie_walkup4");


        zombie_water1 = Textures.textureAtlas.createSprite("zombie_water1");
        zombie_water2 = Textures.textureAtlas.createSprite("zombie_water2");
        zombie_water3 = Textures.textureAtlas.createSprite("zombie_water3");
        zombie_water4 = Textures.textureAtlas.createSprite("zombie_water4");

        zombie_waterup1 = Textures.textureAtlas.createSprite("zombie_waterup1");
        zombie_waterup2 = Textures.textureAtlas.createSprite("zombie_waterup2");
        zombie_waterup3 = Textures.textureAtlas.createSprite("zombie_waterup3");
        zombie_waterup4 = Textures.textureAtlas.createSprite("zombie_waterup4");


        splash1 = Textures.textureAtlas.createSprite("splash1");
        splash2 = Textures.textureAtlas.createSprite("splash2");
        splash3 = Textures.textureAtlas.createSprite("splash3");
        splash4 = Textures.textureAtlas.createSprite("splash4");
        splash5 = Textures.textureAtlas.createSprite("splash5");
        splash6 = Textures.textureAtlas.createSprite("splash6");
        splash7 = Textures.textureAtlas.createSprite("splash7");
        splash8 = Textures.textureAtlas.createSprite("splash8");




        grass = Textures.textureAtlas.createSprite("grass");
        sand = Textures.textureAtlas.createSprite("sand");
        ruff = Textures.textureAtlas.createSprite("ruff");
        green = Textures.textureAtlas.createSprite("green");
        water = Textures.textureAtlas.createSprite("water");
        water2 = Textures.textureAtlas.createSprite("water2");
        water3 = Textures.textureAtlas.createSprite("water3");

        stone = Textures.textureAtlas.createSprite("stone");
        spawner = Textures.textureAtlas.createSprite("spawner");

        palmtree1 = Textures.textureAtlas.createSprite("palmtree1");
        palmtree2 = Textures.textureAtlas.createSprite("palmtree2");

        flag1 = Textures.textureAtlas.createSprite("flag1");
        flag2 = Textures.textureAtlas.createSprite("flag2");
        flag3 = Textures.textureAtlas.createSprite("flag3");
        flag4 = Textures.textureAtlas.createSprite("flag4");
        flag5 = Textures.textureAtlas.createSprite("flag5");

        ball = Textures.textureAtlas.createSprite("ball");
        ball.setScale(0.5f);

        teeoff = Textures.textureAtlas.createSprite("teeoff");

        crosshair = Textures.textureAtlas.createSprite("crosshair");
        particle = Textures.textureAtlas.createSprite("particle");

        bar = Textures.textureAtlas.createSprite("bar");


        light = Textures.textureAtlas.createSprite("light");
        light.setAlpha(0.5f);

        logo = Textures.textureAtlas.createSprite("logo");


        levelMap = new Sprite(Textures.level1);

    }

    public static void setLevelmap(Texture levelmap) {
        Sprites.levelMap = new Sprite(levelmap);
    }
}
