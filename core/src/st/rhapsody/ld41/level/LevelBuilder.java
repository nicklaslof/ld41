package st.rhapsody.ld41.level;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;

public class LevelBuilder {

    public static final int GRASS = 0x00bb00ff;
    public static final int TREE = 0x805000ff;
    public static final int PLAYER = 0xff0000ff;
    public static final int GREEN = 0x00ff00ff;
    public static final int FLAG = 0xbb0000ff;
    public static final int WATER = 0x0000ffff;
    public static final int TEEOFF = 0xff9900ff;
    public static final int SPAWNER = 0xffff00ff;
    public static final int SAND = 0xd2a31eff;
    public static final int RUFF = 0x054c05ff;
    public static final int STONE = 0x696a6aff;

    private final Texture levelTexture;
    private final Level level;

    public LevelBuilder(Texture levelTexture, Level level) {

        this.levelTexture = levelTexture;
        this.level = level;
    }

    public void build() {

        TextureData textureData = levelTexture.getTextureData();

        if (!textureData.isPrepared()){
            textureData.prepare();
        }

        Pixmap pixels = textureData.consumePixmap();

        for (int x = 0; x < levelTexture.getWidth(); x++) {
            for (int y = 0; y < levelTexture.getHeight(); y++) {
                int pixel = pixels.getPixel(x, y);
                int tileY = (levelTexture.getHeight()-1) - y;
                switch(pixel){
                    case GRASS:
                        level.addGrassTile(x,tileY);
                        break;
                    case PLAYER:
                        level.addGreen(x, tileY);
                        level.addPlayer(x, tileY);
                        break;
                    case GREEN:
                        level.addGreen(x, tileY);
                        break;
                    case FLAG:
                        level.addGreen(x, tileY);
                        level.addFlag(x, tileY);
                        break;
                    case WATER:
                        level.addWaterTile(x, tileY);
                        break;
                    case TEEOFF:
                        level.addGreen(x, tileY);
                        level.addTeeOff(x, tileY);
                        break;
                    case SPAWNER:
                        level.addGrassTile(x, tileY);
                        level.addSpawner(x, tileY);
                        break;
                    case SAND:
                        level.addSandTile(x, tileY);
                        break;
                    case RUFF:
                        level.addRuffTile(x, tileY);

                }


            }
        }

        for (int x = 0; x < levelTexture.getWidth(); x++) {
            for (int y = 0; y < levelTexture.getHeight(); y++) {
                int pixel = pixels.getPixel(x, y);
                int tileY = (levelTexture.getHeight() - 1) - y;
                switch (pixel) {
                    case TREE:
                        level.addTreeTile(x, tileY);
                        break;
                    case STONE:
                        level.addStoneTile(x, tileY);
                        break;
                }

            }
        }




    }
}
