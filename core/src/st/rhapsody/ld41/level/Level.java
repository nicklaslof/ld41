package st.rhapsody.ld41.level;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import st.rhapsody.ld41.behaviour.*;
import st.rhapsody.ld41.entity.Entity;
import st.rhapsody.ld41.entity.Light;
import st.rhapsody.ld41.entity.Particle;
import st.rhapsody.ld41.physic.Box2dBuilder;
import st.rhapsody.ld41.physic.Box2dConvert;
import st.rhapsody.ld41.physic.Box2dTemplate;
import st.rhapsody.ld41.physic.PhysicContactListener;
import st.rhapsody.ld41.resource.Sprites;
import st.rhapsody.ld41.resource.Textures;
import st.rhapsody.ld41.screen.FontDrawer;
import st.rhapsody.ld41.tile.Tile;

import static st.rhapsody.ld41.physic.Box2dTemplate.*;

public class Level {

    private static Body knockBackbody;
    private static float knockBackTimer;
    private static Texture texture;
    private static boolean courseChange;
    private static boolean playerDead;
    private static int playerHealthBeforeLevelChange;
    private static int playerShots;
    private static float ballInWaterMessageTimer;
    private static Entity currentBall;
    public static boolean playerFinished;
    private World world;
    private Tile[] backgroundTiles;
    private Tile[] foregroundTiles;
    private static Array<Entity> entities;
    private static Array<Entity> entitiesToRemove;
    private static Array<Particle> particles;
    private static Array<Particle> particlesToRemove;
    private static Entity player;

    private int width;
    private int height;
    private static float backSwingAmmount;

    private static boolean playerShooting;
    private static boolean playerHappy;
    private static int courseNumber;

    private static float courseRenderCounter;

    private static Texture[] levelTextures = new Texture[] { Textures.level1, Textures.level2, Textures.level3,Textures.level4, Textures.level5, Textures.level6,Textures.level7, Textures.level8, Textures.level9};
    private static int score;
    private int playerShoots;
    private static boolean ballMoving;


    public Level(int width, int height) {

        this.width = width;
        this.height = height;
        Level.playerFinished = false;
        score = 0;
        playerHealthBeforeLevelChange = 0;
    }

    public static void setCourse(int number, Texture texture){

        Level.courseNumber = number;
        Level.texture = texture;
        Level.courseChange = true;
        Level.courseRenderCounter = 4.0f;

        Level.playerDead = false;
        Sprites.setLevelmap(texture);
    }

    public static int getPar(){
        switch(courseNumber){
            case 1:
            case 2:
                return 2;
            case 3:
            case 4:
                return 3;
            case 5:
            case 6:
                return 4;
            case 7:
            case 8:
            case 9:
                return 5;
        }

        return 2;
    }

    public static void setPlayerShooting(boolean b){
        playerShooting = b;
    }

    public static boolean isPlayerShooting() {
        return playerShooting;
    }

    public static Vector2 getPlayerPosition() {
        return player.getPosition();
    }

    public static void setBackSwingAmmount(float backSwingAmmount) {
        Level.backSwingAmmount = backSwingAmmount;
    }

    public static void setPlayerHappy(boolean happy) {
        player.setHappy(happy);
        if (happy) {
            calculateScore();
        }
    }

    public static boolean isPlayerHappy() {
        return player.isHappy();
    }

    public static boolean isBallMoving() {
        if (currentBall == null){
            return false;
        }

        //System.out.println(currentBall.getBallSpeed());
        if (currentBall.isBallMoving()){
            return true;
        }

        return false;
    }

    public boolean isPlayerDead() {
        return playerDead;
    }

    private Vector2 tileToPos(int x, int y){
        return new Vector2(x*16, y*16);
    }

    void addPlayer(int x, int y){
        int health = 5;
        if (playerHealthBeforeLevelChange>0){
            health = playerHealthBeforeLevelChange;
        }
            player = new Entity()
                    .addSprite(Sprites.char_walk1)
                    .addWalkSprite(Sprites.char_walk1)
                    .addWalkSprite(Sprites.char_walk2)
                    .addWalkSprite(Sprites.char_walk3)
                    .addWalkSprite(Sprites.char_walk4)
                    .addWalkUpSprite(Sprites.char_walkup1)
                    .addWalkUpSprite(Sprites.char_walkup2)
                    .addWalkUpSprite(Sprites.char_walkup3)
                    .addWalkUpSprite(Sprites.char_walkup4)

                    .addWaterSprite(Sprites.char_water1)
                    .addWaterSprite(Sprites.char_water2)
                    .addWaterSprite(Sprites.char_water3)
                    .addWaterSprite(Sprites.char_water4)
                    .addWaterUpSprite(Sprites.char_waterup1)
                    .addWaterUpSprite(Sprites.char_waterup2)
                    .addWaterUpSprite(Sprites.char_waterup3)
                    .addWaterUpSprite(Sprites.char_waterup4)

                    .addSwingSprite(Sprites.char_swing1)
                    .addSwingSprite(Sprites.char_swing2)
                    .addSwingSprite(Sprites.char_swing3)
                    .addSwingSprite(Sprites.char_swing4)
                    .addSwingUpSprite(Sprites.char_swingup1)
                    .addSwingUpSprite(Sprites.char_swingup2)
                    .addSwingUpSprite(Sprites.char_swingup3)
                    .addSwingUpSprite(Sprites.char_swingup4)
                    .addHappySprite(Sprites.char_happy1)
                    .addHappySprite(Sprites.char_happy2)
                    .withWalkAnimationDelay(0.2f)
                    .withEntityHitCountdown(2.0f)
                    .withHealth(health)
                    .withBehaviour(new PlayerBehaviour())
                    .withBox2dTemplate(new Box2dTemplate()
                            .asType(Box2dTemplate.Type.RECTANGLE)
                            .withWidth(8)
                            .withHeight(8)
                            .filterIsA(PLAYER)
                            .filterCollidesWith((short) (TREE | ZOMBIE | WATER | SENSOR))
                    )


                    .withLight(new Light()
                            .withBehaviour(new LightBehaviour())
                            .withColor(Color.ORANGE.cpy())
                            .withSprite(Sprites.light)
                            .withScale(1.0f)
                            .withLightOffsetX(-10)
                            .withLightOffsetY(4)

                    )


                    .withSpriteOffsetY(-8)
                    .withSpriteOffsetX(-8)
                    .onPosition(tileToPos(x, y));

            player.setIsPlayer();
            entities.add(player);
    }

    void addGrassTile(int x, int y) {
        Tile tile = new Tile()
                .addSprite(Sprites.grass)
                .onPosition(x, y);
        addBackgroundTile(tile, x, y);
    }

    void addSandTile(int x, int y) {
        Tile tile = new Tile()
                .asType(Tile.Type.SAND)
                .addSprite(Sprites.sand)
                .onPosition(x, y);
        addBackgroundTile(tile, x, y);
    }

    void addRuffTile(int x, int y) {
        Tile tile = new Tile()
                .asType(Tile.Type.RUFF)
                .addSprite(Sprites.ruff)
                .onPosition(x, y);
        addBackgroundTile(tile, x, y);
    }

    public void addSpawner(int x, int y) {
        Tile tile = new Tile()
                .addSprite(Sprites.spawner)
                .onPosition(x, y)
                .withBehaviour(new ZombieSpawnBehaviour());

        addForegroundTile(tile, x, y);
    }

    void addWaterTile(int x, int y) {
        Tile tile = new Tile()
                .asType(Tile.Type.WATER)
                .addSprite(Sprites.water)
                .addAnimationSprite(Sprites.water)
                .addAnimationSprite(Sprites.water2)
                .addAnimationSprite(Sprites.water3)
                .addAnimationSprite(Sprites.water2)
                .withAnimationDelay(0.5f)
                .withBox2dTemplate(new Box2dTemplate()
                    .asSensor()
                        .asType(Type.RECTANGLE)
                        .withHeight(8)
                        .withWidth(8)
                        .filterIsA(WATER)
                        .filterCollidesWith((short) (BALL | ZOMBIE | PLAYER))


                )
                .onPosition(x, y);
        addBackgroundTile(tile, x, y);
    }


    public void addGreen(int x, int y) {
        Tile tile = new Tile()
                .addSprite(Sprites.green)
                .onPosition(x, y);
        addBackgroundTile(tile, x, y);
    }

    public void addTeeOff(int x, int y) {
        Tile tile = new Tile()
                .addSprite(Sprites.teeoff)
                .withSpriteOffsetY(+3)
                .withLight(
                        new Light()
                        .withSprite(Sprites.light)
                        .withColor(new Color(0x3333ffff))
                        .onPosition(tileToPos(x, y))
                        .withBehaviour(new LightBehaviour())
                )
                .onPosition(x, y);

        addForegroundTile(tile, x, y);
    }


    public void addFlag(int x, int y) {
        Tile tile = new Tile()
                .asType(Tile.Type.FLAG)
                .addSprite(Sprites.flag1)
                .addAnimationSprite(Sprites.flag1)
                .addAnimationSprite(Sprites.flag2)
                .addAnimationSprite(Sprites.flag3)
                .addAnimationSprite(Sprites.flag4)
                .addAnimationSprite(Sprites.flag5)
                .withAnimationDelay(0.1f)
                .withLight(
                        new Light()
                                .withSprite(Sprites.light)
                                .withColor(new Color(0xff0000ff))
                                .onPosition(tileToPos(x, y))
                                .withBehaviour(new LightBehaviour())
                        .withLightOffsetY(11)
                )
                .withBox2dTemplate(new Box2dTemplate()
                        .asType(Box2dTemplate.Type.RECTANGLE)
                        .withWidth(10)
                        .withHeight(10)
                        .asSensor()
                        .filterIsA(FLAG)
                        .filterCollidesWith(BALL)
                )
                .onPosition(x, y);
        addForegroundTile(tile, x, y);
    }

    void addTreeTile(int x, int y) {
        Tile tile = new Tile()
                .addSprite(Sprites.palmtree1)
                .addAnimationSprite(Sprites.palmtree1)
                .addAnimationSprite(Sprites.palmtree2)
                .withAnimationDelay(1.5f)
                .withBox2dTemplate(new Box2dTemplate()
                        .asStatic()
                    .asType(Box2dTemplate.Type.TREE)
                        .withRestitution(1.0f)
                        .filterIsA(TREE)
                        .filterCollidesWith((short) (PLAYER | ZOMBIE | BALL))

                )
                .onPosition(x, y)
                .withSpriteOffsetX(-12);

        addForegroundTile(tile, x, y);

        Tile leftTile = getBackgroundTile(x - 1, y);
        Tile rightTile = getBackgroundTile(x + 1, y);

        if (leftTile != null){
            if (leftTile.getFirstSprite().equals(Sprites.sand)){
                addSandTile(x,y);
            }
            else if (leftTile.getFirstSprite().equals(Sprites.grass)){
                addGrassTile(x,y);
            }
            else if (leftTile.getFirstSprite().equals(Sprites.ruff)){
                addRuffTile(x,y);
            }
            else if (leftTile.getFirstSprite().equals(Sprites.water)){
                addWaterTile(x,y);
            }

        }else if (rightTile != null){
            if (rightTile.getFirstSprite().equals(Sprites.sand)){
                addSandTile(x,y);
            }
            else if (rightTile.getFirstSprite().equals(Sprites.grass)){
                addGrassTile(x,y);
            }
            else if (rightTile.getFirstSprite().equals(Sprites.ruff)){
                addRuffTile(x,y);
            }
            else if (rightTile.getFirstSprite().equals(Sprites.water)){
                addWaterTile(x,y);
            }
        }else{
            addRuffTile(x, y);
        }

    }

    void addStoneTile(int x, int y) {
        Tile tile = new Tile()
                .addSprite(Sprites.stone)

                .withBox2dTemplate(new Box2dTemplate()
                        .asStatic()
                        .asType(Type.RECTANGLE)
                        .withRestitution(1.0f)
                        .withWidth(16)
                        .withHeight(16)
                        .filterIsA(TREE)
                        .filterCollidesWith((short) (PLAYER | ZOMBIE | BALL))

                )
                .onPosition(x, y)
                .withSpriteOffsetX(0);

        addForegroundTile(tile, x, y);

        Tile leftTile = getBackgroundTile(x - 1, y);
        Tile rightTile = getBackgroundTile(x + 1, y);

        if (leftTile != null){
            if (leftTile.getFirstSprite().equals(Sprites.sand)){
                addSandTile(x,y);
            }
            else if (leftTile.getFirstSprite().equals(Sprites.grass)){
                addGrassTile(x,y);
            }
            else if (leftTile.getFirstSprite().equals(Sprites.ruff)){
                addRuffTile(x,y);
            }
            else if (leftTile.getFirstSprite().equals(Sprites.water)){
                addWaterTile(x,y);
            }

        }else if (rightTile != null){
            if (rightTile.getFirstSprite().equals(Sprites.sand)){
                addSandTile(x,y);
            }
            else if (rightTile.getFirstSprite().equals(Sprites.grass)){
                addGrassTile(x,y);
            }
            else if (rightTile.getFirstSprite().equals(Sprites.ruff)){
                addRuffTile(x,y);
            }
            else if (rightTile.getFirstSprite().equals(Sprites.water)){
                addWaterTile(x,y);
            }
        }else{
            addRuffTile(x, y);
        }

    }


    private void addBackgroundTile(Tile tile, int x, int y){
        backgroundTiles[x + (width*y)] = tile;
    }

    private void addForegroundTile(Tile tile, int x, int y){
        foregroundTiles[x + (width*y)] = tile;
    }

    public Tile getBackgroundTile(int x, int y){
        if (x < 0 || x > width-1 || y < 0 || y > height-1){
            return null;
        }
        //System.out.println(x + " "+y);
        return backgroundTiles[x + ( width* y)];
    }

    public void tick(float delta) {

        if (courseChange){
            initNewCourse();
        }

        if (courseRenderCounter > 0.0f){
            courseRenderCounter -= delta;
        }

        if (courseRenderCounter < 0.0f){
            courseRenderCounter = 0.0f;
        }

        if (ballInWaterMessageTimer > 0.0f){
            ballInWaterMessageTimer -= delta;
        }

        if (ballInWaterMessageTimer < 0.0f){
            ballInWaterMessageTimer = 0.0f;
        }


        world.step(1/60f,1,1);

        if (knockBackTimer > 0.0f){
            knockBackTimer -= delta;
        }

        if (knockBackTimer < 0.0f){
            if (knockBackbody != null){
                world.destroyBody(knockBackbody);
                knockBackbody = null;
            }
            knockBackTimer = 0.0f;
        }

        ZombieBehaviour.tickGlobalGruntSound(delta);


        for (Tile backgroundTile : backgroundTiles) {
            if (backgroundTile != null){
                backgroundTile.tick(delta);
            }
        }

        for (Tile foregroundTile : foregroundTiles) {
            if (foregroundTile != null){
                foregroundTile.tick(delta);
            }
        }

        for (Entity entity : entities) {
            entity.tick(delta);
            if (entity.isDisposed()){
                if (entity.getBody() != null) {
                    world.destroyBody(entity.getBody());
                }

                if (entity.getSensor() != null) {
                    world.destroyBody(entity.getSensor());
                }
                entitiesToRemove.add(entity);
            }
        }

        for (Particle particle : particles) {
            particle.tick(delta);
            if (particle.isDisposed()){
                particlesToRemove.add(particle);
            }
        }


        if (entitiesToRemove.size > 0){
            entities.removeAll(entitiesToRemove, true);
            entitiesToRemove.clear();
        }

        if (particlesToRemove.size > 0){
            particles.removeAll(particlesToRemove, true);
            particlesToRemove.clear();
        }


    }

    private void initNewCourse() {

        if (world != null){
            for (Entity entity : entities) {
                if (entity.getBody() != null) {
                    world.destroyBody(entity.getBody());
                }

                if (entity.getSensor() != null) {
                    world.destroyBody(entity.getSensor());
                }
            }


            for (Tile backgroundTile : backgroundTiles) {
                if (backgroundTile != null){
                    if (backgroundTile.getBody() != null){
                        world.destroyBody(backgroundTile.getBody());
                    }
                }
            }

            for (Tile foregroundTile : foregroundTiles) {
                if (foregroundTile != null){
                    if (foregroundTile.getBody() != null){
                        world.destroyBody(foregroundTile.getBody());
                    }
                }
            }

            world.dispose();
        }

        world = new World(new Vector2(0,0), true);

        world.setContactListener(new PhysicContactListener());

        Box2dBuilder.initalize(world);



        entities =  new Array<Entity>();
        entitiesToRemove =  new Array<Entity>();

        particles =  new Array<Particle>();
        particlesToRemove =  new Array<Particle>();

        backgroundTiles = new Tile[width*height];
        foregroundTiles = new Tile[width*height];

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                addGrassTile(x, y);
            }
        }

        LevelBuilder levelBuilder = new LevelBuilder(texture, this);
        levelBuilder.build();

        setPlayerHappy(false);
        setPlayerShooting(true);
        playerShots = 0;
        courseChange = false;
    }

    public void render(SpriteBatch spriteBatch) {
        for (Tile backgroundTile : backgroundTiles) {
            if (backgroundTile != null){
                backgroundTile.render(spriteBatch);
            }
        }

        for (Tile foregroundTile : foregroundTiles) {
            if (foregroundTile != null){
                foregroundTile.render(spriteBatch);
            }
        }

        for (Entity entity : entities) {
            entity.render(spriteBatch);
        }

        for (Particle particle : particles) {
            particle.render(spriteBatch);
        }
    }

    public void updateCamera(OrthographicCamera camera) {
        if (player != null) {

            float camX = player.getPosition().x;
            float camY = player.getPosition().y + 30;

            if (camX < 200) {
                camX = 200;
            }

            if (camX > 310) {
                camX = 310;
            }


            if (camY < 150) {
                camY = 150;
            }

            if (camY > 870) {
                camY = 870;
            }

            camera.position.set(camX, camY, 0);
            camera.update();
        }

    }

    public static void shootBall(Vector2 source, Vector2 direction, float hitVelocity) {
        if (hitVelocity > 0.07f){
            hitVelocity = 0.07f;
        }

        increasePlayerShots();
        Vector2 srcPos = source.cpy();
        //srcPos.add(8,14);
        Entity ball = new Entity()
                .addSprite(Sprites.ball)
                .withBehaviour(new BallBehaviour(srcPos, direction, hitVelocity))
                .withBox2dTemplate(
                        new Box2dTemplate()
                        .asType(Box2dTemplate.Type.CIRCLE)
                        .withRadius(3f)
                        .withDensity(2.0f)
                        .withFriction(2.0f)
                        .withRestitution(1.0f)
                        .filterIsA(BALL)
                        .filterCollidesWith((short) (ZOMBIE | TREE | FLAG | WATER))


                )
                .withScale(0.5f)
                .withSpriteOffsetX(-2)
                .withSpriteOffsetY(1)
                .withLight(new Light()
                    .withBehaviour(new LightBehaviour())
                        .withColor(Color.WHITE.cpy())
                        .withScale(0.3f)
                        .withSprite(Sprites.light)
                        .withLightOffsetX(-2)
                        .withLightOffsetY(+6)

                )
                .onPosition(srcPos);

        ball.setIsBall();
        currentBall = ball;
        entities.add(ball);
    }

    public Entity getPlayer() {
        return player;
    }

    public void debugRender(Box2DDebugRenderer debugRenderer, Matrix4 debugCameraMatrix) {
        debugRenderer.render(world, debugCameraMatrix);
    }

    public static void spawnZombies(Vector2 position, int number) {
        for (int i = 0; i < number; i++) {
            Entity zombie = new Entity()
                    .addSprite(Sprites.zombie_walk1)
                    .addWalkSprite(Sprites.zombie_walk1)
                    .addWalkSprite(Sprites.zombie_walk2)
                    .addWalkSprite(Sprites.zombie_walk3)
                    .addWalkSprite(Sprites.zombie_walk4)
                    .addWalkUpSprite(Sprites.zombie_walkup1)
                    .addWalkUpSprite(Sprites.zombie_walkup2)
                    .addWalkUpSprite(Sprites.zombie_walkup3)
                    .addWalkUpSprite(Sprites.zombie_walkup4)
                    .addWaterSprite(Sprites.zombie_water1)
                    .addWaterSprite(Sprites.zombie_water2)
                    .addWaterSprite(Sprites.zombie_water3)
                    .addWaterSprite(Sprites.zombie_water4)
                    .addWaterUpSprite(Sprites.zombie_waterup1)
                    .addWaterUpSprite(Sprites.zombie_waterup2)
                    .addWaterUpSprite(Sprites.zombie_waterup3)
                    .addWaterUpSprite(Sprites.zombie_waterup4)
                    .withWalkAnimationDelay(0.2f)
                    .withHealth(1)
                    .withBehaviour(new ZombieBehaviour())
                    .withBox2dTemplate(new Box2dTemplate()
                            .asType(Box2dTemplate.Type.RECTANGLE)
                            .withWidth(8)
                            .withHeight(8)
                            .withDensity(0.1f)
                            .filterIsA(ZOMBIE)
                            .filterCollidesWith((short) (TREE | PLAYER | ZOMBIE | WATER | KNOCKBACK | BALL))


                    )
                    .withSpriteOffsetY(-8)
                    .withSpriteOffsetX(-8)
                    .withLight(new Light()
                        .withColor(Color.BLUE.cpy())
                            .withScale(1.0f)
                            .withSprite(Sprites.light)
                        .withBehaviour(
                                new LightBehaviour())


                    )
                    .onPosition(new Vector2(position.x+ MathUtils.random(-32,32), position.y+MathUtils.random(-32,32)));

            zombie.setWalking(true);
            zombie.setEnemy(true);
            entities.add(zombie);
        }

    }

    public void renderBackSwing(SpriteBatch batch) {
        Sprites.bar.setPosition(-380,-200);
        Sprites.bar.setScale(5);
        Sprites.bar.setOrigin(0,0);
        float v = backSwingAmmount * 4000;
        //System.out.println(v);
        Sprites.bar.setColor(Color.GREEN);

        if (v > 100){
            Sprites.bar.setColor(Color.YELLOW);
        }

        if (v > 200){
            Sprites.bar.setColor(Color.ORANGE);
        }

        if (v > 260){
            Sprites.bar.setColor(Color.RED);
        }


        Sprites.bar.setScale(5,Math.max(1,v));
        Sprites.bar.draw(batch);
    }

    public static void spawnParticles(Vector2 position, float ttl, int number, Color color) {
        for (int i = 0; i < number; i++) {
            Particle particle = new Particle()
                    .onPosition(position)
                    .withBehaviour(new GrassParticleBehaviour(color.cpy()))
                    .withTTL(ttl)
                    .withSprite(Sprites.particle);
            particles.add(particle);

        }
    }

    public static void spawnSplash(Vector2 position){


        Entity splash = new Entity()
                .addSprite(Sprites.splash1)
                .addWalkSprite(Sprites.splash1)
                .addWalkSprite(Sprites.splash2)
                .addWalkSprite(Sprites.splash3)
                .addWalkSprite(Sprites.splash4)
                .addWalkSprite(Sprites.splash5)
                .addWalkSprite(Sprites.splash6)
                .addWalkSprite(Sprites.splash7)
                .addWalkSprite(Sprites.splash8)
                .onPosition(position)
                .withWalkAnimationDelay(0.1f)
                .withLight(new Light()
                    .withColor(Color.WHITE.cpy())
                    .withSprite(Sprites.light)
                    .onPosition(position)
                        .withScale(0.5f)
                    .withBehaviour(new LightBehaviour()))
                .withSpriteOffsetX(-8)
                .withSpriteOffsetY(-8)
                .playOnce();

        splash.setWalking(true);
        entities.add(splash);
    }

    public void renderLights(SpriteBatch lightSpriteBatch) {
        for (Tile backgroundTile : backgroundTiles) {
            if (backgroundTile != null){
                backgroundTile.renderLight(lightSpriteBatch);
            }
        }

        for (Tile foregroundTile : foregroundTiles) {
            if (foregroundTile != null){
                foregroundTile.renderLight(lightSpriteBatch);
            }
        }

        for (Entity entity : entities) {
            entity.renderLight(lightSpriteBatch);
        }
    }

    public static void addKnockBack(Vector2 position, Vector2 direction) {
        direction.scl(0.20f);
        if (knockBackbody == null) {
            Box2dTemplate box2dTemplate = new Box2dTemplate()
                    .asType(Type.RECTANGLE)
                    .withWidth(2)
                    .withHeight(2)
                    .withDensity(2.0f)
                    .filterIsA(KNOCKBACK)
                    .filterCollidesWith(ZOMBIE);

            knockBackbody = Box2dBuilder.build(box2dTemplate);
            knockBackbody.setTransform(Box2dConvert.toBox(position.x), Box2dConvert.toBox(position.y), 0);
            knockBackbody.setUserData(new Knockback());
            knockBackbody.applyForceToCenter(direction, true);

            knockBackTimer = 0.15f;
        }

    }

    public void renderCourseName(SpriteBatch uiBatch) {
        if (courseRenderCounter > 0.0f) {
            FontDrawer.drawTextAt(uiBatch, "COURSE " + courseNumber, -60, 100, false, Align.left);
        }
    }
    public void renderBallInWater(SpriteBatch uiBatch) {
        if (ballInWaterMessageTimer > 0.0f) {
            FontDrawer.drawTextAt(uiBatch, "THE BALL HIT THE WATER! TRY AGAIN", -260, 120, false, Align.left);
        }
    }

    public static void prepareLevelChange() {
        playerHealthBeforeLevelChange = player.getHealth();
        if (courseNumber+1 > levelTextures.length){
            courseNumber = 0;
            Level.playerFinished();
        }

        setCourse(courseNumber+1, levelTextures[courseNumber]);
    }

    private static void playerFinished() {
        playerFinished = true;
    }

    public boolean isPlayerFinished() {
        return playerFinished;
    }

    private static void calculateScore(){
        int courseScore = getPar() - playerShots;
        score -= courseScore;
    }

    public static void setPlayerDead() {
        playerDead = true;
    }

    public static void healPlayer() {
        player.heal();
    }

    public int getScore() {
        return score;
    }

    public int getPlayerShots() {
        return playerShots;
    }

    public static void increasePlayerShots(){
        playerShots++;
    }

    public static void setBallInWater() {
        ballInWaterMessageTimer = 4.0f;

    }


}
