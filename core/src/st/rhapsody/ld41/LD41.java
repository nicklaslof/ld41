package st.rhapsody.ld41;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import st.rhapsody.ld41.resource.Sounds;
import st.rhapsody.ld41.resource.Sprites;
import st.rhapsody.ld41.resource.Textures;
import st.rhapsody.ld41.screen.FontDrawer;
import st.rhapsody.ld41.screen.GameScreen;
import st.rhapsody.ld41.screen.InstructionsScreen;
import st.rhapsody.ld41.screen.IntroScreen;

public class LD41 extends Game {

	private static Screen currentScreen;
	private static boolean changeScreen;
	private Textures textures;
	private Sprites sprites;
	private FontDrawer fontDrawer;
	private Sounds sounds;

	@Override
	public void create() {

		textures = new Textures();
		sprites = new Sprites();
		fontDrawer = new FontDrawer();
		sounds = new Sounds();

		switchToIntroScreen();
		//switchToGameScreen();

	}

	@Override
	public void render() {
		super.render();
		if (changeScreen){
			setScreen(currentScreen);
			changeScreen = false;
		}
	}

	public static void switchToGameScreen(){
		currentScreen = new GameScreen();
		changeScreen = true;
	}

	public static void switchToIntroScreen(){
		currentScreen = new IntroScreen();
		changeScreen = true;
	}

	public static void switchToInstructionScreen() {
		currentScreen = new InstructionsScreen();
		changeScreen = true;
	}
}
