package st.rhapsody.ld41.behaviour;

import st.rhapsody.ld41.level.Level;
import st.rhapsody.ld41.tile.Tile;

public class ZombieSpawnBehaviour extends TileBehaviour{


    private float spawnCounter = 15.0f;

    @Override
    public void tick(float delta, Tile tile) {
        super.tick(delta, tile);

        if (spawnCounter > 0.0f){
            spawnCounter -= delta;
        }

        if (spawnCounter < 0.0f){
            spawnCounter = 0.0f;
        }

        if (spawnCounter == 0.0f){
            Level.spawnZombies(tile.getPosition().cpy(), 1);
            spawnCounter = 5.0f;
        }

    }
}
