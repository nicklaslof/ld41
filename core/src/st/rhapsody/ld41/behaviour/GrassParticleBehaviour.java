package st.rhapsody.ld41.behaviour;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld41.entity.Particle;

public class GrassParticleBehaviour extends ParticleBehaviour{
    private final Vector2 movementVector;

    public GrassParticleBehaviour(Color color) {
        super(color);
        movementVector = new Vector2(MathUtils.random(-0.2f, 0.2f), MathUtils.random(-0.2f, 0.2f));
    }

    @Override
    public void tick(double delta, Particle particle) {
        super.tick(delta, particle);

        particle.transform(movementVector.x, movementVector.y);
        scale /= 1.02f;
    }
}
