package st.rhapsody.ld41.behaviour;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import st.rhapsody.ld41.entity.Entity;
import st.rhapsody.ld41.level.Level;
import st.rhapsody.ld41.resource.Sounds;


import static st.rhapsody.ld41.physic.Box2dConvert.toBox;
import static st.rhapsody.ld41.physic.Box2dConvert.toWorld;

public class BallBehaviour extends Behaviour{
    private final Vector2 source;
    private final Vector2 direction;
    private boolean newBall;
    private float hitVelocity;
    private float currentVelocity;
    private boolean landed = false;
    private boolean ballMoving;

    public BallBehaviour(Vector2 source, Vector2 direction, float hitVelocity) {

        this.source = source;
        this.direction = direction;
        this.hitVelocity = hitVelocity;
        this.currentVelocity = hitVelocity;
        this.newBall = true;
        this.direction.scl(hitVelocity*5f);
    }

    @Override
    public void tick(Entity entity, float delta) {
        super.tick(entity, delta);
        Body body = entity.getBody();


        if (newBall){
            //body.applyLinearImpulse(new Vector2(toBox(direction.x), toBox(direction.y)), Vector2.Zero, true);
            newBall = false;
        }
        if (!landed){
            body.applyLinearImpulse(new Vector2(toBox(direction.x), toBox(direction.y)), Vector2.Zero, true);
        }
        Vector2 linearVelocity = body.getLinearVelocity();

        if (landed){
            linearVelocity.scl(0.9f);
        }else{
            linearVelocity.scl(0.95f);
        }
        body.setLinearVelocity(linearVelocity.x, linearVelocity.y);

        float len = linearVelocity.len();


        if (len > 10.0f){
            Vector2 scl = linearVelocity.nor();
            body.setLinearVelocity(scl);
        }

        if (len > 0.0f){
            ballMoving = true;
        }else{
            ballMoving = false;
        }


        if (currentVelocity > 0.0f) {
            currentVelocity -= delta/10.0f;

            if (currentVelocity > hitVelocity/2.0f){
                entity.scale(delta*4.0f);
            }else{
                entity.scale(-delta*4.0f);
            }






        }else{
            currentVelocity = 0.0f;
        }

        if (!landed && currentVelocity == 0.0f){
            landed = true;
            Sounds.landingBall.play(1.0f);
            //System.out.println("LANDED!");
            Level.spawnParticles(entity.getPosition(), 2.0f, 10,new Color(0x5fbf00ff));
            //Level.spawnZombies(entity.getPosition());
            entity.addSensor();
        }







        /*if (Math.abs(linearVelocity.x) < 1f && Math.abs(linearVelocity.y) < 1f ) {
            body.applyLinearImpulse(new Vector2(toBox(direction.x), toBox(direction.y)), Vector2.Zero, true);
        }

        body.setLinearDamping(20f);

        */

        /*if (newBall){
            //
            //entity.getBody().applyForceToCenter(toBox(direction.x), toBox(direction.y),true);
            newBall = false;
        }else{

            if (!landed) {
                body.applyForceToCenter(toBox(direction.x), toBox(direction.y), true);

            }



            if (landed){
                linearVelocity.scl(53.0f*delta);
            }else{
                linearVelocity.scl(56.9f*delta);
            }

            body.setLinearVelocity(linearVelocity.x, linearVelocity.y);






        }

        if (currentVelocity > 0.0f) {
            currentVelocity -= delta/10.0f;

            if (currentVelocity > hitVelocity/2.0f){
                entity.scale(delta*4.0f);
            }else{
                entity.scale(-delta*4.0f);
            }

        }else{
            currentVelocity = 0.0f;
        }

        if (!landed && currentVelocity == 0.0f){
            landed = true;
            //System.out.println("LANDED!");
            Level.spawnParticles(entity.getPosition(), 2.0f, 10,new Color(0x5fbf00ff));
            //Level.spawnZombies(entity.getPosition());
            entity.addSensor();
        }


        float maxSpeed = 0.005f;

        //System.out.println(linearVelocity);

        if (toWorld(linearVelocity.x) > maxSpeed){
            System.out.println(toWorld(linearVelocity.x));
            linearVelocity.set(toBox(maxSpeed), linearVelocity.y);
        }

        if (toWorld(linearVelocity.y) > maxSpeed){
            System.out.println(toWorld(linearVelocity.y));
            linearVelocity.set(linearVelocity.x, toBox(maxSpeed));
        }

        if (toWorld(linearVelocity.x) < -maxSpeed){
            linearVelocity.set(toBox(-maxSpeed), linearVelocity.y);
        }

        if (toWorld(linearVelocity.y) < -maxSpeed){
            linearVelocity.set(linearVelocity.x, toBox(-maxSpeed));
        }

        //System.out.println(linearVelocity);*/
    }

    public boolean hasLanded() {
        return this.landed;
    }

    public float getSpeed() {
        return currentVelocity;
    }

    public boolean isBallMoving(){
        return ballMoving;
    }
}
