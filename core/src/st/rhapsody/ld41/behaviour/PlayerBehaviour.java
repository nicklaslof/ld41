package st.rhapsody.ld41.behaviour;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld41.entity.Entity;
import st.rhapsody.ld41.level.Level;
import st.rhapsody.ld41.resource.Sounds;
import st.rhapsody.ld41.screen.GameScreen;

public class PlayerBehaviour extends Behaviour{

    private float hitVelocity;
    private float playerVelocity = 40.0f;
    private boolean countDownHitVelocity = false;
    private Vector2 calcVector = new Vector2();
    private float shootTimer;
    private boolean shootBall;
    private float hitCountdown;
    private float hitTimer;
    private boolean hitEnemy;

    private float happyMoveTimer;
    private boolean happyMoveNegative;
    private float happyLoops;
    private float stopPlayerCounter;

    @Override
    public void tick(Entity entity, float delta) {
        super.tick(entity, delta);

        boolean nothingPressed = true;

        if (entity.isHappy()){

            if (happyMoveTimer > 0.0f){
                happyMoveTimer -= delta;
            }
            if (happyMoveTimer <= 0.0f){
                happyMoveNegative = !happyMoveNegative;
                happyMoveTimer = 1.0f;
                happyLoops++;
            }

            if (happyMoveNegative) {
                entity.transform(-playerVelocity, 0);
            }else {
                entity.transform(playerVelocity, 0);
            }

            if (happyLoops >= 6){
                Level.prepareLevelChange();
            }

        }


        if (hitCountdown > 0.0f){
            hitCountdown -= delta;
        }

        if (hitCountdown < 0.0f){
            hitCountdown = 0.0f;
        }



        if (stopPlayerCounter > 0.0f){
            stopPlayerCounter -= delta;
        }

        if (stopPlayerCounter < 0.0f){
            stopPlayerCounter = 0.0f;
        }

        if (Level.isPlayerShooting()){

            if (countDownHitVelocity){
                hitVelocity -= delta/30.0f;
            }else {
                hitVelocity += delta / 30.0f;
            }

            if (hitVelocity <= 0.0f){
                hitVelocity = 0.0f;
                countDownHitVelocity = false;
            }

            if (hitVelocity >= 0.07f){
                hitVelocity = 0.07f;
                countDownHitVelocity = true;
            }




            Level.setBackSwingAmmount(Math.min(0.07f,hitVelocity));
            entity.setSwing(true);
            entity.enableAnimations();
        }

        if (hitVelocity == 0.0f && !Level.isPlayerShooting()) {
            //entity.setWalking(false);
            //entity.setWalkingUp(false);

            boolean canPlayerMove = true;
            if (Level.isBallMoving() && stopPlayerCounter == 0.0f){
                canPlayerMove = true;
            }else if (Level.isBallMoving()){
                canPlayerMove = false;
            }

            if (canPlayerMove) {
                if (GameScreen.inputController.isRightPressed()) {
                    entity.transform(playerVelocity, 0);

                    if (!GameScreen.inputController.isUpPressed()) {
                        entity.setWalkingUp(false);
                        entity.setWalking(true);
                    }
                    if (entity.isFlippedLeft()) {
                        entity.flipRight();
                    }
                    nothingPressed = false;
                    entity.enableAnimations();
                }

                if (GameScreen.inputController.isLeftPressed()) {
                    entity.transform(-playerVelocity, 0);

                    if (!GameScreen.inputController.isUpPressed()) {
                        entity.setWalkingUp(false);
                        entity.setWalking(true);
                    }
                    if (entity.isFlippedRight()) {
                        entity.flipLeft();
                    }
                    nothingPressed = false;
                    entity.enableAnimations();
                }

                if (GameScreen.inputController.isUpPressed()) {
                    entity.transform(0, playerVelocity);
                    entity.setWalking(false);
                    entity.setWalkingUp(true);
                    nothingPressed = false;
                    entity.enableAnimations();
                }

                if (GameScreen.inputController.isDownPressed()) {
                    entity.transform(0, -playerVelocity);
                    entity.setWalking(true);
                    entity.setWalkingUp(false);
                    nothingPressed = false;
                    entity.enableAnimations();
                }

                if (nothingPressed) {
                    entity.stop();
                    if (!entity.isHappy() && !entity.isSwingShooting()) {
                        entity.stopAllAnimations();
                    }
                }
            }else{
                entity.stop();
                if (!entity.isHappy() && !entity.isSwingShooting()) {
                    entity.stopAllAnimations();
                }
            }
        }else{
            Vector2 mousePositionWorldSpace = GameScreen.mousePositionWorldSpace;

            Vector2 playerPosition = Level.getPlayerPosition();

            calcVector.set(mousePositionWorldSpace).sub(playerPosition);

            if (calcVector.y > 0){
                entity.setWalkingUp(true);
            }else{
                entity.setWalkingUp(false);
            }

            if (calcVector.x > 0) {
                entity.flipRight();
            }else{
                entity.flipLeft();
            }


        }


        if (!entity.isInWater() && hitVelocity == 0.0f && GameScreen.inputController.isSpacePressed()){
            if (hitCountdown == 0.0f) {
                hitTimer = 0.2f;
                hitEnemy = true;
                entity.setSwingShoot(true);
                entity.enableAnimations();
            }


        }

        if (hitEnemy && hitTimer > 0.0f){
            hitTimer -=delta;
        }

        if (hitEnemy && hitTimer < 0.0f){

            Vector2 mousePositionWorldSpace = GameScreen.mousePositionWorldSpace;

            Vector2 playerPosition = Level.getPlayerPosition();


            calcVector.set(mousePositionWorldSpace).sub(playerPosition).nor();
            Level.addKnockBack(entity.getPosition(), calcVector.cpy());
            if (!Level.isPlayerHappy()) {
                Sounds.swing.play(1.0f);
            }
            hitCountdown = 0.5f;
            hitTimer = 0.0f;
            hitEnemy = false;
        }

        if (hitVelocity > 0.00f && GameScreen.inputController.isSpacePressed() && !shootBall){
            shootBall = true;
            shootTimer = 0.4f;

            Level.setBackSwingAmmount(0.0f);
            entity.setSwing(false);
            entity.setSwingShoot(true);
            Level.setPlayerShooting(false);

        }

        if (shootBall && shootTimer > 0.0f){
            shootTimer -=delta;
        }

        if (shootBall && shootTimer < 0.0f){
            Vector2 playerPos = entity.getPosition().cpy();
            //System.out.println(hitVelocity);
            //playerPos.sub(8,14);
            if (entity.isInSand()){
                Sounds.swingSand.play(1.0f);
                Level.spawnParticles(new Vector2(entity.getPosition().x, entity.getPosition().y-8), 2.0f, 20, new Color(0xfff392ff));
            }else {
                Sounds.swingGrass.play(1.0f);
            }
            Vector2 direction = GameScreen.mousePositionWorldSpace.cpy().sub(playerPos).nor();
            float distance = Math.abs(playerPos.cpy().dst(GameScreen.mousePositionWorldSpace.cpy()));

            if (hitVelocity < 0.02f){
                hitVelocity = 0.0215f;
            }
            Level.shootBall(playerPos, direction, hitVelocity);

            hitVelocity = 0.0f;
            shootBall = false;
            shootTimer = 0.0f;
            stopPlayerCounter = 2.0f;
        }


    }

    public void setPlayerVelocity(int playerVelocity) {
        this.playerVelocity = playerVelocity;
    }
}
