package st.rhapsody.ld41.behaviour;

import com.badlogic.gdx.graphics.Color;
import st.rhapsody.ld41.entity.Entity;
import st.rhapsody.ld41.entity.Particle;

public class ParticleBehaviour {
    protected Color color;
    protected float scale = 1.0f;

    public ParticleBehaviour(Color color) {
        this.color = color;
    }

    public void tick(double delta, Particle particle){

    }

    public Color getColor() {
        return color;
    }

    public float getScale() {
        return scale;
    }
}