package st.rhapsody.ld41.behaviour;

import com.badlogic.gdx.math.Vector2;
import st.rhapsody.ld41.entity.Entity;
import st.rhapsody.ld41.level.Level;
import st.rhapsody.ld41.resource.Sounds;

import static st.rhapsody.ld41.physic.Box2dConvert.toBox;
import static st.rhapsody.ld41.physic.Box2dConvert.toWorld;

public class ZombieBehaviour extends Behaviour {

    private Vector2 directionVector = new Vector2();
    private Vector2 myPosition = new Vector2();
    private boolean gruntPlayed;

    private static float globalGruntSoundCounter;

    public static void tickGlobalGruntSound(float delta){
        if (globalGruntSoundCounter > 0.0f){
            globalGruntSoundCounter -= delta;
        }

        if (globalGruntSoundCounter < 0.0f){
            globalGruntSoundCounter = 0.0f;
        }
    }

    @Override
    public void tick(Entity entity, float delta) {
        if (!Level.isPlayerHappy()) {



            directionVector.set(Level.getPlayerPosition());
            myPosition.set(entity.getPosition());


            float distanceToPlayer = Level.getPlayerPosition().dst(entity.getPosition());


            if (distanceToPlayer < 150) {
                if (!gruntPlayed && globalGruntSoundCounter == 0.0f){
                    Sounds.zombieGrunt.play(0.5f);
                    globalGruntSoundCounter = 1f;
                }
                gruntPlayed = true;

                directionVector = directionVector.sub(myPosition).nor();
                entity.enableAnimations();

                entity.transform(directionVector.x, directionVector.y);

                if (directionVector.x > 0.0f) {
                    entity.flipRight();
                } else {
                    entity.flipLeft();
                }

                if (directionVector.y > 0.0f) {
                    entity.setWalkingUp(true);
                } else {
                    entity.setWalkingUp(false);
                }
            }else{
                entity.stop();
                entity.stopAllAnimations();
                gruntPlayed = false;
            }

        }else{
            entity.stop();
            entity.stopAllAnimations();
        }

        //entity.getBody().applyForceToCenter(toBox(directionVector.x), toBox(directionVector.y),true);


    }
}
