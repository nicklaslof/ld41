package st.rhapsody.ld41.tile;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Array;
import st.rhapsody.ld41.behaviour.Behaviour;
import st.rhapsody.ld41.behaviour.TileBehaviour;
import st.rhapsody.ld41.behaviour.ZombieSpawnBehaviour;
import st.rhapsody.ld41.entity.Entity;
import st.rhapsody.ld41.entity.Light;
import st.rhapsody.ld41.physic.Box2dBuilder;
import st.rhapsody.ld41.physic.Box2dTemplate;

import static st.rhapsody.ld41.physic.Box2dConvert.toBox;
import static st.rhapsody.ld41.physic.Box2dConvert.toWorld;

public class Tile {

    private Array<Sprite> sprites = new Array<Sprite>();
    private Array<Sprite> animationSprite = new Array<Sprite>();
    private int tileX;
    private int tileY;
    private Body body;

    private float spriteOffsetX;
    private float spriteOffsetY;
    private float animationDelay;
    private float timer;
    private int animationFrame;
    private Sprite spriteToDraw;
    private Light light;

    private Type type = Type.NEUTRAL;
    private TileBehaviour behaviour;

    private Vector2 position = new Vector2();

    public Tile withBehaviour(TileBehaviour behaviour) {
        this.behaviour = behaviour;
        return this;
    }

    public Body getBody() {
        return body;
    }

    public enum Type{
        WATER,
        SAND,
        GREEN,
        GRASS,
        FLAG,
        RUFF,
        NEUTRAL
    }


    public Sprite getFirstSprite(){
        return sprites.first();
    }

    public Tile addSprite(Sprite sprite){
        sprites.add(sprite);
        this.spriteToDraw = sprite;
        return this;
    }

    public Tile addAnimationSprite(Sprite sprite) {
        this.animationSprite.add(sprite);
        return this;
    }

    public Tile withAnimationDelay(float animationDelay) {
        this.animationDelay = animationDelay;
        this.timer = MathUtils.random(0,animationDelay-0.1f);
        return this;
    }

    public void tick(float delta) {
        if (behaviour != null){
            behaviour.tick(delta, this);
        }
        timer += delta;
        if (light != null){
            light.tick(delta);
        }
        if (timer > animationDelay) {
            timer = 0.0f;

            if (animationDelay > 0.0) {
                spriteToDraw = animationSprite.get(animationFrame);

                animationFrame++;

                if (animationFrame > animationSprite.size - 1) {
                    animationFrame = 0;
                }
            }
        }
    }

    public void render(SpriteBatch spriteBatch) {
        /*for (Sprite sprite : sprites) {
            sprite.setPosition((tileX*16)+spriteOffsetX, (tileY*16)+spriteOffsetY);
            sprite.draw(spriteBatch);
        }*/
        if (spriteToDraw != null) {
            spriteToDraw.setPosition((tileX * 16) + spriteOffsetX, (tileY * 16) + spriteOffsetY);
            spriteToDraw.draw(spriteBatch);
        }

    }

    public void renderLight(SpriteBatch lightBatch){
        if (light != null){
            light.render(lightBatch);
        }
    }

    public Tile onPosition(int x, int y) {
        if (this.body != null){
            this.body.setTransform(
                    new Vector2(
                            toBox((x*16) + (sprites.first().getWidth()/2)),
                            toBox((y*16) + (sprites.first().getHeight()/2))),0);
        }
        this.tileX = x;
        this.tileY = y;

        this.position.set(tileX*16, tileY*16);
        return this;
    }

    public Tile withBox2dTemplate(Box2dTemplate box2dTemplate) {
        this.body = Box2dBuilder.build(box2dTemplate);
        this.body.setUserData(this);
        return this;
    }

    public Tile withSpriteOffsetX(float x){
        this.spriteOffsetX = x;
        return this;
    }

    public Tile withSpriteOffsetY(float y){
        this.spriteOffsetY = y;
        return this;
    }

    public Tile withLight(Light light) {
        this.light = light;
        return this;
    }

    public Tile asType(Type type){
        this.type = type;
        return this;
    }

    public Type getType() {
        return type;
    }

    public Vector2 getPosition() {
        return position;
    }
}
