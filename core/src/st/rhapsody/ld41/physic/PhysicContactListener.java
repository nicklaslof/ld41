package st.rhapsody.ld41.physic;

import com.badlogic.gdx.physics.box2d.*;
import st.rhapsody.ld41.entity.Entity;
import st.rhapsody.ld41.level.Knockback;
import st.rhapsody.ld41.resource.Sounds;
import st.rhapsody.ld41.tile.Tile;

public class PhysicContactListener implements ContactListener {
    @Override
    public void beginContact(Contact contact) {
        Fixture fixtureA = contact.getFixtureA();
        Fixture fixtureB = contact.getFixtureB();

        Tile tileA = null;
        Tile tileB = null;

        Entity entityA = null;
        Entity entityB = null;

        Knockback knockbackA = null;
        Knockback knockbackB = null;

        if (fixtureA.getBody().getUserData() instanceof Tile){
            tileA = (Tile) fixtureA.getBody().getUserData();
        }

        if (fixtureB.getBody().getUserData() instanceof Tile){
            tileB = (Tile) fixtureB.getBody().getUserData();
        }

        if (fixtureA.getBody().getUserData() instanceof Entity){
            entityA = (Entity) fixtureA.getBody().getUserData();
        }

        if (fixtureB.getBody().getUserData() instanceof Entity){
            entityB = (Entity) fixtureB.getBody().getUserData();
        }

        if (fixtureA.getBody().getUserData() instanceof Knockback){
            knockbackA = (Knockback) fixtureA.getBody().getUserData();
        }

        if (fixtureB.getBody().getUserData() instanceof Knockback){
            knockbackB = (Knockback) fixtureB.getBody().getUserData();
        }

        if (tileA != null && entityB != null){
            entityB.collideWithTile(tileA);
        }

        if (tileB != null && entityA != null){
            entityA.collideWithTile(tileB);
        }

        //System.out.println(entityA + " "+entityB + "  "+knockbackA + "  "+knockbackB);


        if (entityA != null && entityB != null){
            entityA.collideWithEntity(entityB);
        }

        if (entityB != null && entityA != null){
            entityB.collideWithEntity(entityA);
        }

        if (knockbackA != null && entityB != null){
            entityB.hit(1);
           // Sounds.zombiehit.play(1.0f);
            fixtureA.getBody().setUserData(null);
        }

        if (knockbackB != null && entityA != null){
            entityA.hit(1);
            //Sounds.zombiehit.play(1.0f);
            fixtureB.getBody().setUserData(null);
        }





        /*if (fixtureB.isSensor()){
            //System.out.println("!!!!!"+fixtureA.getBody().getUserData());
        }

        if (fixtureA.isSensor()){
            Entity ball = (Entity) fixtureB.getBody().getUserData();
            ball.collidedWithFlag();
        }*/
    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
