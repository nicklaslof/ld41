package st.rhapsody.ld41.physic;

public class Box2dConvert {
    public static float WORLD_TO_BOX = .01f;
    public static float BOX_TO_WORLD = 100f;

    public static float toBox(float x){
        return x * WORLD_TO_BOX;
    }

    public static float toWorld(float x){
        return x * BOX_TO_WORLD;
    }
}
