package st.rhapsody.ld41.physic;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

import static st.rhapsody.ld41.physic.Box2dConvert.toBox;
import static st.rhapsody.ld41.physic.Box2dConvert.toWorld;

public class Box2dBuilder {

    private static World world;

    public static void initalize(World world) {
        Box2dBuilder.world = world;
    }


    public static Body build(Box2dTemplate box2dTemplate) {

        switch(box2dTemplate.getType()){
            case CIRCLE:
                return buildCircle(box2dTemplate);
            case RECTANGLE:
                return buildRectangle(box2dTemplate);
            case TREE:
                return buildTree(box2dTemplate);
        }


        return null;
    }

    private static Body buildTree(Box2dTemplate physicsTemplate) {
        Body body = getBody(physicsTemplate);

        PolygonShape polygonShape = new PolygonShape();

        polygonShape.setAsBox(toBox(4), toBox(22)
                , new Vector2(toBox(-14f), toBox(0f)), 0f);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.isSensor = physicsTemplate.hasSensor();
        fixtureDef.density = physicsTemplate.getDensity();
        fixtureDef.friction = physicsTemplate.getFriction();
        fixtureDef.restitution = physicsTemplate.getRestitution();
        fixtureDef.shape = polygonShape;
        fixtureDef.filter.categoryBits = physicsTemplate.getCategoryFilter();
        fixtureDef.filter.maskBits = physicsTemplate.getMaskFilter();
        body.createFixture(fixtureDef);


        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(Box2dConvert.toBox(18));
        circleShape.setPosition(new Vector2(Box2dConvert.toBox(-14),Box2dConvert.toBox(7)));
        fixtureDef = new FixtureDef();
        fixtureDef.shape = circleShape;
        fixtureDef.density = physicsTemplate.getDensity();
        fixtureDef.friction = physicsTemplate.getFriction();
        fixtureDef.restitution = physicsTemplate.getRestitution();
        fixtureDef.filter.categoryBits = physicsTemplate.getCategoryFilter();
        fixtureDef.filter.maskBits = physicsTemplate.getMaskFilter();


        body.createFixture(fixtureDef);



        return body;

    }

    private static Body buildRectangle(Box2dTemplate physicsTemplate) {
        Body body = getBody(physicsTemplate);

        PolygonShape polygonShape = new PolygonShape();

        polygonShape.setAsBox(toBox(physicsTemplate.getHeight()), toBox(physicsTemplate.getWidth())
                , new Vector2(toBox(0f), toBox(0f)), 0f);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.isSensor = physicsTemplate.hasSensor();
        fixtureDef.density = physicsTemplate.getDensity();
        fixtureDef.friction = physicsTemplate.getFriction();
        fixtureDef.restitution = physicsTemplate.getRestitution();
        fixtureDef.shape = polygonShape;
        fixtureDef.filter.categoryBits = physicsTemplate.getCategoryFilter();
        fixtureDef.filter.maskBits = physicsTemplate.getMaskFilter();
        body.createFixture(fixtureDef);

        return body;


    }

    private static Body buildCircle(Box2dTemplate box2dTemplate) {
        Body body = getBody(box2dTemplate);

        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(Box2dConvert.toBox(box2dTemplate.getRadius()));
        circleShape.setPosition(new Vector2(0,Box2dConvert.toBox(box2dTemplate.getRadius())));
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = circleShape;
        fixtureDef.density = box2dTemplate.getDensity();
        fixtureDef.friction = box2dTemplate.getFriction();
        fixtureDef.restitution = box2dTemplate.getRestitution();
        fixtureDef.filter.categoryBits = box2dTemplate.getCategoryFilter();
        fixtureDef.filter.maskBits = box2dTemplate.getMaskFilter();

        if (box2dTemplate.hasSensor()){
            fixtureDef.isSensor = true;
        }

        body.createFixture(fixtureDef);

        return body;
    }









    private static Body getBody(Box2dTemplate physicsTemplate) {
        BodyDef bodyDef = createBodyDef(physicsTemplate);
        bodyDef.fixedRotation = true;
        Body body = world.createBody(bodyDef);
        return body;
    }


    private static BodyDef createBodyDef(Box2dTemplate physicsTemplate) {
        BodyDef bodyDef = new BodyDef();
        if (physicsTemplate.isKinematic()) {
            bodyDef.type = BodyDef.BodyType.KinematicBody;
        }else if (physicsTemplate.isStatic()){
            bodyDef.type = BodyDef.BodyType.StaticBody;
        }else{
            bodyDef.type = BodyDef.BodyType.DynamicBody;
        }
        return bodyDef;
    }



}
