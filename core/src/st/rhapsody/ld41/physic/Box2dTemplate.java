package st.rhapsody.ld41.physic;

public class Box2dTemplate {

    private short categoryFilter;
    private short maskFilter;


    public enum Type{
        RECTANGLE,
        CIRCLE,
        TREE
    }

    public static short PLAYER = 1;
    public static short ZOMBIE = 2;
    public static short BALL = 4;
    public static short TREE = 8;
    public static short FLAG = 16;
    public static short WATER = 32;
    public static short SENSOR = 64;
    public static short KNOCKBACK = 128;


    private Type type;
    private boolean kinematic;
    private boolean staticBody;
    private boolean sensor;
    private float radius;
    private int width;
    private int height;
    private float density = 1.0f;
    private float friction = 0.1f;
    private float restitution = 0.0f;

    public boolean isKinematic() {
        return kinematic;
    }

    public boolean isStatic() {
        return staticBody;
    }

    public boolean hasSensor() {
        return sensor;
    }

    public Box2dTemplate asSensor(){
        this.sensor = true;
        return this;
    }

    public float getRadius() {
        return radius;
    }

    public Box2dTemplate withWidth(int width) {
        this.width = width;
        return this;
    }

    public Box2dTemplate withHeight(int height) {
        this.height = height;
        return this;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public float getDensity() {
        return density;
    }

    public float getFriction() {
        return friction;
    }

    public float getRestitution() {
        return restitution;
    }

    public Box2dTemplate withDensity(float density) {
        this.density = density;
        return this;
    }

    public Box2dTemplate withFriction(float friction) {
        this.friction = friction;
        return this;
    }

    public Box2dTemplate withRestitution(float restitution) {
        this.restitution = restitution;
        return this;
    }

    public Box2dTemplate filterIsA(short categoryFilter) {
        this.categoryFilter = categoryFilter;
        return this;
    }

    public Box2dTemplate filterCollidesWith(short maskFilter) {
        this.maskFilter = maskFilter;
        return this;
    }

    public Box2dTemplate asKinematic(){
        this.kinematic = true;
        return this;
    }

    public Box2dTemplate asStatic(){
        this.staticBody = true;
        return this;
    }

    public Box2dTemplate asType(Type type){
        this.type = type;
        return this;
    }

    public Type getType() {
        return type;
    }

    public Box2dTemplate withRadius(float radius){
        this.radius = radius;
        return this;
    }


    public short getCategoryFilter() {
        return categoryFilter;
    }

    public short getMaskFilter() {
        return maskFilter;
    }
}
